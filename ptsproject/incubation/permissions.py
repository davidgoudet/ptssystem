from rest_framework import permissions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes

class UserCanViewUserDetails(permissions.BasePermission):

	def has_permission(self, request, view):
		token = Token.objects.get(key=response.data['token'])
		user = User.objects.get(id=token.user_id)
		user_permissions_set = user.userprofile.user_permission_group.permission_set

		for permission in user_permissions_set:
			if (permission.name == "UserCanViewUserDetails"):
				return True

		return False