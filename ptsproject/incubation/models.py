from django.db import models
from django.contrib.auth.models import User
from PIL import Image

class Role(models.Model):
	name = models.CharField(max_length=100)

class Level(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	image = models.CharField(max_length=200,null=True)
	imageFileSystem = models.CharField(max_length=200,null=True)

class Template(models.Model):
	type_of_company = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	code = models.TextField(blank=True,null=True)

class StageTemplate(models.Model):
	name = models.CharField(max_length=100)
	created = models.DateTimeField(auto_now_add=True)
	description = models.TextField(blank=True)
	externalFilesFolder = models.CharField(max_length=500,null=True)
	level = models.ForeignKey(Level,related_name='stage_level')
	template = models.ForeignKey(Template, related_name='stage_template',on_delete=models.SET_NULL, null=True)

class Badge(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	details_list = models.TextField(blank=True)
	image = models.CharField(max_length=200,null=True)
	imageFileSystem = models.CharField(max_length=200,null=True)
	
class Company(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	president = models.ForeignKey(User, related_name='company_president',on_delete=models.SET_NULL, null=True)
	image = models.CharField(max_length=200,null=True)
	imageFileSystem = models.CharField(max_length=200,null=True)
	filesFolder = models.CharField(max_length=200,null=True)
	externalFilesFolder = models.CharField(max_length=500,null=True)
	filesFolderFileSystem = models.CharField(max_length=200,null=True)
	legalRepresentative = models.CharField(max_length=500,null=True)
	businessName = models.CharField(max_length=500,null=True)
	graduated = models.BooleanField(default=False)
	badges = models.ManyToManyField(Badge)
	level = models.ForeignKey(Level, related_name='company_level',on_delete=models.SET_NULL, null=True)
	template = models.ForeignKey(Template, related_name='company_template',on_delete=models.SET_NULL, null=True)
	current_stage_template = models.ForeignKey(StageTemplate, related_name='company_current_stage_template',on_delete=models.SET_NULL, null=True)

class PermissionGroup(models.Model):
	name = models.CharField(max_length=100, unique=True)
	description = models.TextField(blank=True)

class Permission(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	permissionGroup = models.ManyToManyField(PermissionGroup)

class UserProfile(models.Model):
	profile_picture = models.CharField(max_length=200,null=True)
	sex = models.CharField(max_length=5,null=True)
	profession = models.CharField(max_length=100,null=True)
	permissionGroup = models.ForeignKey(PermissionGroup, related_name='user_permission_group', on_delete=models.SET_NULL, null=True)
	position = models.CharField(max_length=100,null=True)
	is_in_board = models.BooleanField(default=False)
	role = models.ManyToManyField(Role)
	badges = models.ManyToManyField(Badge)
	company = models.ForeignKey(Company,related_name='user_company',null=True)
	linked_companies = models.ManyToManyField(Company)
	user = models.OneToOneField(
		User,
		on_delete=models.CASCADE,
		default='userprofile'
	)

class StageStory(models.Model):
	start_date = models.DateTimeField(null=True)
	ending_date = models.DateTimeField(null=True)
	experience_points = models.PositiveIntegerField(null=True)
	approved = models.BooleanField(default=False)
	template = models.ForeignKey(StageTemplate,related_name='stage_template',null=True)
	company = models.ForeignKey(Company,related_name='stage_company',null=True)

class Milestone(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	points = models.PositiveIntegerField(null=True)
	badge = models.ForeignKey(Badge,related_name='milestone_badge')

class TaskArea(models.Model):
	name = models.CharField(max_length=100)	

class Status(models.Model):
	name = models.CharField(max_length=100)

class TaskTemplate(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	acceptance_criteria = models.TextField(blank=True)
	stage = models.ForeignKey(StageTemplate,related_name='task_stage', null=True)
	milestone = models.ForeignKey(Milestone, related_name='task_milestone',on_delete=models.SET_NULL, null=True)

class TaskStory(models.Model):
	start_date = models.DateTimeField(null=True)
	ending_date = models.DateTimeField(null=True)
	delivery_file = models.CharField(max_length=200,null=True)
	story_have_not_template = models.BooleanField(default=False)
	name_not_template = models.CharField(max_length=100, null=True)
	description_not_template = models.TextField(blank=True)
	stage_not_template = models.ForeignKey(StageTemplate,related_name='task_story_stage', null=True)
	deliveryFileSystem = models.CharField(max_length=200,null=True)
	delivery_text = models.TextField(blank=True)
	accepted = models.BooleanField(default=False)
	sent = models.BooleanField(default=False)
	sent_date = models.DateTimeField(auto_now=True,null=True)
	points = models.PositiveIntegerField(null=True)
	comments = models.TextField(blank=True)
	status = models.ForeignKey(Status,related_name='task_story_status', on_delete=models.SET_NULL,null=True)
	area = models.ForeignKey(TaskArea,related_name='task_area_story', on_delete=models.SET_NULL,null=True)
	company = models.ForeignKey(Company, related_name='company_task_story',on_delete=models.SET_NULL, null=True)
	task = models.ForeignKey(TaskTemplate, related_name='task_template',on_delete=models.SET_NULL, null=True)
	in_charge = models.ForeignKey(User, related_name='task_in_charge',on_delete=models.SET_NULL, null=True)
	creator = models.ForeignKey(User, related_name='task_creator',on_delete=models.SET_NULL, null=True)

class Challenge(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	start_date = models.DateTimeField(null=True)
	ending_date = models.DateTimeField(null=True)
	accepted = models.BooleanField(default=False)
	deliveryFileSystem = models.CharField(max_length=200,null=True)
	delivery_file = models.CharField(max_length=200,null=True)
	delivery_text = models.TextField(blank=True)
	sent = models.BooleanField(default=False)
	comments = models.TextField(blank=True)
	sent_date = models.DateTimeField(auto_now=True,null=True)
	sender = models.ForeignKey(User, related_name='sender_challenge',on_delete=models.SET_NULL, null=True)
	receiver = models.ForeignKey(Company, related_name='receiver_challenge',on_delete=models.SET_NULL, null=True)
	in_charge = models.ForeignKey(User, related_name='challenge_in_charge',on_delete=models.SET_NULL, null=True)
	badge = models.ForeignKey(Badge, related_name='challenge_badge',on_delete=models.SET_NULL, null=True)

class Pending(models.Model):
	sent = models.DateTimeField(auto_now_add=True)
	reviewed = models.BooleanField(default=False)
	revision_date = models.DateTimeField(null=True)
	task_story = models.ForeignKey(TaskStory, related_name='pending_task',on_delete=models.SET_NULL, null=True)


class BusinessSummary(models.Model):
	company = models.OneToOneField(
		Company,
		on_delete=models.CASCADE,
		related_name='company_summary'
	)

class ScreenshotCompany(models.Model):
	company = models.ForeignKey(Company, related_name='screenshot_company',on_delete=models.SET_NULL, null=True)
	created = models.DateTimeField(auto_now_add=True)
	number_of_stages_approved = models.PositiveIntegerField(null=True)
	employees_number = models.PositiveIntegerField(null=True)
	employees_salary = models.PositiveIntegerField(null=True)

class ScreenshotIncubator(models.Model):	
	created = models.DateTimeField(auto_now_add=True)
	companies_number = models.PositiveIntegerField(null=True)
	earnings = models.PositiveIntegerField(null=True)


#********* CHAT MODELS ***********#
def _thumbnail(img, size):
    width, height = img.size
    
    if width > height:
        delta = width - height
        left = int(delta/2)
        upper = 0
        right = height + left
        lower = height
    else:
        delta = height - width
        left = 0
        upper = int(delta/2)
        right = width
        lower = width + upper
    
    img = img.crop((left, upper, right, lower))
    img.thumbnail(size, Image.ANTIALIAS)
    return img

class Room(models.Model):
    name = models.CharField(max_length=32)
    createdBy = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.name


class Message(models.Model):
    content = models.TextField(max_length=1000)
    responseTo = models.ForeignKey("Message", blank=True, null=True, related_name='responses')
    room = models.ForeignKey(Room)
    writer = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.content[:30]

class UserExtra(models.Model):
    user = models.OneToOneField(User)
    img = models.ImageField(upload_to='images', verbose_name="Select Your Profile Image", blank=True, null=True)
    thumbnail = models.ImageField(upload_to='images', blank=True, null=True)
    
    def save(self):
        try:
            this = UserExtra.objects.get(user = self.user)
            self.id = this.id #force update instead of insert
            if self.img and this.img != self.img:
                os.remove(this.img.path)
                if this.thumbnail:
                    os.remove(this.thumbnail.path)
        except:
            pass
        if self.img:
            super(UserExtra, self).save()
            tsize = 35,35
            isize = 700,700
            fname, ext = os.path.splitext(self.img.name)
            outfilepath = os.path.splitext(self.img.path)[0] + ".thumbnail" + ext
            outfilefield = fname + ".thumbnail" + ext
            im = Image.open(self.img.path)
            im = _thumbnail(im, tsize)
            im.save(self.img.path, im.format)
            im.thumbnail(isize, Image.ANTIALIAS)
            im.save(outfilepath, im.format)
            self.thumbnail = outfilefield
        super(UserExtra, self).save()

    def __unicode__(self):
        return self.user.username

#**************** CHAT MODELS ***************#