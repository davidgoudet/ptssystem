# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-16 13:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incubation', '0015_auto_20170316_1312'),
    ]

    operations = [
        migrations.AddField(
            model_name='pending',
            name='revision_date',
            field=models.DateTimeField(null=True),
        ),
    ]
