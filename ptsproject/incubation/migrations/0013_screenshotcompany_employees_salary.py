# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-01 21:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incubation', '0012_auto_20170501_1906'),
    ]

    operations = [
        migrations.AddField(
            model_name='screenshotcompany',
            name='employees_salary',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
