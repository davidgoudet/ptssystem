# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-03 15:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incubation', '0016_auto_20170503_1449'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenge',
            name='comments',
            field=models.TextField(blank=True),
        ),
    ]
