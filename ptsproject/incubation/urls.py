from django.conf.urls import url, include
from incubation import views
from django.contrib.auth.models import User
from incubation.serializers import *
from rest_framework import routers
from rest_framework import generics
from incubation.models import *
from incubation.permissions import *



urlpatterns = [
   	
	
	url(r'^users/', views.Users.as_view(), name='user-list'),
	url(r'^user-detail/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),
	url(r'^companies/', views.Companies.as_view(), name='company-list'),
	url(r'^company-detail/(?P<pk>[0-9]+)/$', views.CompanyDetail.as_view(), name='company-detail'),
	url(r'^company/', views.CompanyCreate.as_view(), name='company-create'),
	url(r'^companies-mentor/', views.CompaniesMentor.as_view(), name='company-list'),
	url(r'^stages/(?P<pk>[0-9]+)/$', views.Stages.as_view(), name='stages-list'),
	url(r'^stage-detail/(?P<pk>[0-9]+)/$', views.StageDetail.as_view(), name='stage-detail'),
	url(r'^stage-read/(?P<pk>[0-9]+)/$', views.StageRead.as_view(), name='stage-detail'),
	url(r'^all-stages/', views.AllStages.as_view(), name='all-stages'),
	url(r'^stage/', views.StageCreate.as_view(), name='stage-create'),
	url(r'^templates/', views.Templates.as_view(), name='template-list'),
	url(r'^template/', views.TemplatesCreate.as_view(), name='template-create'),
	url(r'^template-detail/(?P<pk>[0-9]+)/$', views.TemplateDetail.as_view(), name='template-detail'),
	url(r'^tasks/(?P<pk>[0-9]+)/$', views.Tasks.as_view(), name='tasks-list'),
	url(r'^task/', views.TaskCreate.as_view(), name='task-create'),
	url(r'^all-tasks/', views.AllTasks.as_view(), name='all-tasks'),
	url(r'^task-detail/(?P<pk>[0-9]+)/$', views.TaskDetail.as_view(), name='task-detail'),
	url(r'^levels/', views.Levels.as_view(), name='levels-list'),
	url(r'^level-detail/(?P<pk>[0-9]+)/$', views.LevelDetail.as_view(), name='level-detail'),
	url(r'^level/', views.LevelCreate.as_view(), name='level-create'),
	url(r'^level-read/(?P<pk>[0-9]+)/$', views.LevelRead.as_view(), name='level-read'),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),	
	url(r'^challenges/', views.Challenges.as_view(), name='challenge-list'),
	url(r'^challenges-sent/', views.ChallengesSent.as_view(), name='challenge-sent-list'),
	url(r'^challenge/', views.ChallengesCreate.as_view(), name='challenge-create'),
	url(r'^challenge-detail/(?P<pk>[0-9]+)/$', views.ChallengeDetail.as_view(), name='challenge-detail'),
	url(r'^challenge-review/(?P<pk>[0-9]+)/$', views.ChallengeReview.as_view(), name='challenge-review'),
	url(r'^badges/', views.Badges.as_view(), name='badges-list'),
	url(r'^badge-detail/(?P<pk>[0-9]+)/$', views.BadgeDetail.as_view(), name='badge-detail'),
	url(r'^badge/', views.BadgesCreate.as_view(), name='badge-create'),
	url(r'^badges-users-or-company/(?P<pk>[0-9]+)/$', views.BadgesUserCompany.as_view(), name='badges-users-or-company'),
	url(r'^task-story-detail/(?P<pk>[0-9]+)/$', views.TaskStoryDetail.as_view(), name='task-story-detail'),
	url(r'^task-story-review/(?P<pk>[0-9]+)/$', views.TaskStoryReview.as_view(), name='task-story-review'),
	url(r'^task-story/', views.TaskStoryCreate.as_view(), name='task-story-create'),
	url(r'^task-stories-stage-company/(?P<cy>[0-9]+)/(?P<co>[0-9]+)/$', views.TaskStoriesStageCompany.as_view(), name='task-stories-stage-company'),
	url(r'^task-stories-user/(?P<us>[0-9]+)/(?P<st>[0-9]+)/$', views.TaskStoriesUser.as_view(), name='task-stories-user'),
	url(r'^task-stories-incubator-employee/(?P<us>[0-9]+)/$', views.TaskStoriesIncubatorEmployee.as_view(), name='task-stories-incubator-employee'),	
	url(r'^pending/', views.PendingAll.as_view(), name='pending-list'),
	url(r'^pending-challenges/', views.PendingChallenges.as_view(), name='pending-challenges-list'),	
	url(r'^files-management/', views.FilesManagement.as_view(), name='files-management'),
	url(r'^task-areas/', views.TaskAreas.as_view(), name='task-area-list'),
	url(r'^status/', views.StatusView.as_view(), name='status-list'),
	url(r'^screenshoots-company/(?P<pk>[0-9]+)/$', views.ScreenshotCompanyView.as_view(), name='screenshoots-companies-list'),
	url(r'^screenshoot-company-detail/(?P<pk>[0-9]+)/$', views.ScreenshotCompanyDetail.as_view(), name='screenshoot-company-detail'),
	url(r'^screenshoot-company/(?P<pk>[0-9]+)/$', views.ScreenshotCompanyCreate.as_view(), name='screenshoot-company-create'),
	url(r'^screenshoots-incubator/', views.ScreenshotIncubatorView.as_view(), name='screenshoots-incubators-list'),
	url(r'^screenshoot-incubator-detail/(?P<pk>[0-9]+)/$', views.ScreenshotIncubatorDetail.as_view(), name='screenshoot-incubator-detail'),
	url(r'^screenshoot-incubator/', views.ScreenshotIncubatorCreate.as_view(), name='screenshoot-incubator-create'),
	url(r'^rooms/', views.RoomListing.as_view(), name='room-list'),
	url(r'^room-delete/(?P<pk>[0-9]+)/$', views.RoomDelete.as_view(), name='room-delete'),
	url(r'^room-create/', views.RoomCreate.as_view(), name='room-create'),
	url(r'^permission-groups/', views.PermissionGroups.as_view(), name='permission-groups-list'),
	url(r'^user-profiles/', views.UserProfiles.as_view(), name='user-profiles-list'),
	url(r'^user-profile-detail/(?P<pk>[0-9]+)/$', views.UserProfileDetail.as_view(), name='user-profile-detail'),
	url(r'^get-user-profile/(?P<pk>[0-9]+)/$', views.GetUserProfile.as_view(), name='get-user-profile'),
	url(r'^number-graduated-companies/', views.NumberGraduatedCompanies.as_view(), name='number-graduated-companies'),
	url(r'^total-number-employees/', views.TotalNumberEmployees.as_view(), name='total-number-employees'),
	url(r'^total-salary/', views.TotalSalary.as_view(), name='total-salary'),
	url(r'^incubator-earnings/', views.IncubatorEarnings.as_view(), name='incubator-earnings'),
	url(r'^logs/', views.Logs.as_view(), name='logs-list'),
	url(r'^company-points/(?P<pk>\d+)/$', views.CompanyPoints.as_view(), name='company-points'),
	url(r'^room/(?P<id>\d+)/$', views.RoomView.as_view()),
    url(r'^room/(?P<id>\d+)/(?P<page>\d+)/$', views.RoomView.as_view()),
    url(r'^user$', views.UserView.as_view()),
    url(r'^lobby$', views.LobbyView.as_view()),
    url(r'^main$', views.MainView.as_view()),
    url(r'^message$', views.MessageView.as_view()),

]