from django.contrib.auth.models import User, Group
from incubation.models import *
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="incubation:user-detail")
    userprofile = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups', 'first_name', 'last_name','userprofile')

class LevelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Level
        fields = ('name','description','image','id') 

class TemplateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Template
        fields = ('type_of_company', 'description', 'code', 'id')

class CompanySerializer(serializers.HyperlinkedModelSerializer):
    level = LevelSerializer(read_only=True)
    template = TemplateSerializer(read_only=True)
    current_stage_template = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Company
        fields = ('name', 'description', 'image', 'level', 'template', 'id', 'current_stage_template', 'filesFolder', 'externalFilesFolder')

class PermissionGroupSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = PermissionGroup
        fields = ('name','description','id')

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    permissionGroup = PermissionGroupSerializer(read_only=True)
    company = CompanySerializer(read_only=True)
    linked_companies = CompanySerializer(read_only=True, many=True)

    class Meta:
        model = UserProfile
        fields = ('profile_picture', 'sex', 'user', 'permissionGroup', 'id', 'company', 'linked_companies')


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class StageSerializer(serializers.HyperlinkedModelSerializer):
    template = serializers.PrimaryKeyRelatedField(read_only=True)
    level = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = StageTemplate
        fields = ('name', 'created', 'description', 'template', 'id', 'level', 'externalFilesFolder')

class TaskAreaSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = TaskArea
        fields = ('name','id')   

class StatusSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = Status
        fields = ('name', 'id')        

class TaskSerializer(serializers.HyperlinkedModelSerializer):
    stage = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = TaskTemplate
        fields = ('name', 'description','stage','id')

class TaskStorySerializer(serializers.HyperlinkedModelSerializer):
    company = serializers.PrimaryKeyRelatedField(read_only=True)
    task = TaskSerializer(read_only=True)
    in_charge = UserSerializer(read_only=True)
    creator = UserSerializer(read_only=True)
    area = TaskAreaSerializer(read_only=True)
    status = StatusSerializer(read_only=True)
    stage_not_template = StageSerializer(read_only=True)

    class Meta:
        model = TaskStory
        fields = ('delivery_file', 'delivery_text','id','company','task','sent','start_date','ending_date','in_charge','creator','area', 'status','points','story_have_not_template','name_not_template','description_not_template','stage_not_template', 'sent_date', 'comments', 'accepted')
       
class BadgeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Level
        fields = ('name','description','image','id')

class ChallengeSerializer(serializers.HyperlinkedModelSerializer):
    badge = BadgeSerializer(read_only=True)
    in_charge = UserSerializer(read_only=True)

    class Meta:
        model = Challenge
        fields = ('name','description','badge', 'id', 'in_charge', 'ending_date', 'sent', 'sent_date', 'accepted', 'delivery_text', 'delivery_file', 'comments')  
          

class PendingSerializer(serializers.HyperlinkedModelSerializer):
    task_story = TaskStorySerializer(read_only=True)
    
    class Meta:
        model = Pending
        fields = ('sent','reviewed','revision_date','task_story')  


class ScreenshotCompanySerializer(serializers.HyperlinkedModelSerializer):
    company = CompanySerializer(read_only=True)
    class Meta:
        model = ScreenshotCompany
        fields = ('company','created','employees_number','id', 'number_of_stages_approved')    

class ScreenshotIncubatorSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = ScreenshotIncubator
        fields = ('created','companies_number','id', 'earnings')        
       
        


#********** CHAT SERIALIZERS ***********#
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.fields import Field
from incubation.models import Room, Message


class UserHandler(object):
    def full_name(self, obj):
        return obj.get_full_name() or obj.username
    
    def get_img_url(self, obj):
        try:
            ue = obj.userextra
            if ue.thumbnail:
                return obj.userextra.thumbnail.url
        except:
            pass
        #move to settings
        return '/chat/img/anonymous.png'

    def short_name(self, obj):
        try:
            n = obj.get_short_name()
        except:
            n = None
        return n or obj.username


class LobbySerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('name',)


class MsgUserSerializer(UserHandler, serializers.Serializer):
    name = serializers.SerializerMethodField('full_name')
    img = serializers.SerializerMethodField('get_img_url')
    class Meta:
        model = User
        fields = ('name', 'img', 'id')


class UserSerializerChat(UserHandler, serializers.ModelSerializer):
    name = serializers.SerializerMethodField('full_name')
    short_name = serializers.SerializerMethodField('short_name')
    img = serializers.SerializerMethodField('get_img_url')

    class Meta:
        model = User
        fields = ('username', 'name', 'short_name', 'img')

class RoomsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('id', 'name')

class RoomSerializer(serializers.ModelSerializer):
    writer = MsgUserSerializer()
    class Meta:
        model = Message
        fields = ('id', 'content', 'writer')

class SubMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'content', 'room', 'writer')

class MessageSerializer(serializers.ModelSerializer):
    writer = MsgUserSerializer(read_only=True)
    responseTo = SubMessageSerializer(read_only=True)
    class Meta:
        model = Message
        fields = ('id', 'content', 'responseTo', 'room', 'writer')

#********** CHAT SERIALIZERS ***********#