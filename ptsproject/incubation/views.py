from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from incubation.models import *
from rest_framework import generics
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from incubation.serializers import *
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from django.core import serializers
from rest_framework.request import Request

import datetime
import json
import os
import shutil
import stat

from incubation.forms import AccountForm
from incubation.serializers import RoomSerializer, LobbySerializer, UserSerializer, MessageSerializer, RoomsSerializer
from rest_framework import views

from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponseForbidden
from django.core.paginator import Paginator, EmptyPage
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login as contrib_login
from django.conf import settings


class Globals:
    def baseURL():
        return "http://pts.hezionstudios.com/sistema/"
    def baseURLBackend():
        return "http://pts.hezionstudios.com/"
    def baseURLFileSystem():
        return '/home/pts_hezion_user/pts.hezionstudios.com/public/sistema/'

class PermissionsManagement:
    def checkUserPermission(permission, user):
        listOfPermissions = user.userprofile.permissionGroup.permission_set.all()
        for permission_object in listOfPermissions:
            if permission_object.name == permission:
                return True

        return False


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class Users(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CompanySerializer
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Company.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            company = Company.objects.get(id=self.kwargs['pk'])
            company.delete()
            if company.imageFileSystem!=None:
                f = open(company.imageFileSystem, 'w')
                f.close()

                if os.path.isfile(company.image):
                    os.remove(f.name)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            
            company = Company.objects.get(id=pk)
            

            
            if (len(request.FILES) != 0):

                level = Level.objects.get(id=request.data['company[level]'])
                template = Template.objects.get(id=request.data['company[template]'])
                current_stage_template = StageTemplate.objects.get(id=request.data['company[current_stage_template]'])
            
                company.level = level
                company.template = template        
                if 'company[externalFilesFolder]' in request.data:
                    company.externalFilesFolder = request.data['company[externalFilesFolder]']   
                company.current_stage_template = current_stage_template 
                company.name=request.data['company[name]']
                company.description=request.data['company[description]']
                company.save()

                #Instantiating Tasks into TaskStories
                for task in company.current_stage_template.task_stage.all():
                    if (not task.task_template.filter(company=company).exists()):
                        task_story = TaskStory(company=company, task=task)
                        task_story.save()



                my_file = request.FILES['file']
                if company.imageFileSystem!=None:
                    f = open(company.imageFileSystem, 'w')
                    f.close()
                if company.image!=None:
                    if os.path.isfile(company.image):
                        os.remove(f.name)

                filename = Globals.baseURLFileSystem()+'dist/img/companies/'+str(company.id)
                with open(filename, 'wb+') as temp_file:
                    for chunk in my_file.chunks():
                        temp_file.write(chunk)

                company.image = Globals.baseURL()+'dist/img/companies/'+str(company.id)
                company.imageFileSystem = Globals.baseURLFileSystem()+'dist/img/companies/'+str(company.id)
                company.save()
            else:
                level = Level.objects.get(id=request.data['level'])
                template = Template.objects.get(id=request.data['template'])
                current_stage_template = StageTemplate.objects.get(id=request.data['current_stage_template'])

                company.level = level
                company.template = template    
                company.current_stage_template = current_stage_template
                company.name=request.data['name']
                company.description=request.data['description']
                company.save()

                #Instantiating Tasks into TaskStories
                for task in company.current_stage_template.task_stage.all():
                    if (not task.task_template.filter(company=company).exists()):
                        task_story = TaskStory(company=company, task=task)
                        task_story.save()

            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class CompanyCreate(generics.ListCreateAPIView):
    parser_classes = (MultiPartParser, FormParser,)
    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            
            company = Company(name=request.data['company[name]'],description=request.data['company[description]'])
            if 'company[externalFilesFolder]' in request.data:
                company.externalFilesFolder = request.data['company[externalFilesFolder]']

            if 'company[businessName]' in request.data:
                company.businessName = request.data['company[businessName]']

            if 'company[legalRepresentative]' in request.data:
                company.legalRepresentative = request.data['company[legalRepresentative]']

            company.save()
            my_file = request.FILES['file']
            filename = Globals.baseURLFileSystem()+'dist/img/companies/'+str(company.id)
            with open(filename, 'wb+') as temp_file:
                for chunk in my_file.chunks():
                    temp_file.write(chunk)
            
            #newpath = Globals.baseURLFileSystem()+'dist/files/companies/'+str(company.id)
            #if not os.path.exists(newpath):
            #    os.makedirs(newpath)

            #company.filesFolderFileSystem = Globals.baseURLFileSystem()+'dist/files/companies/'+str(company.id)
            #company.filesFolder = 'dist/files/companies/'+str(company.id)

            company.image = Globals.baseURL()+'dist/img/companies/'+str(company.id)
            company.imageFileSystem = Globals.baseURLFileSystem()+'dist/img/companies/'+str(company.id)
            company.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class Companies(generics.ListCreateAPIView):
    serializer_class = CompanySerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Company.objects.order_by('name')
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class CompaniesMentor(generics.ListCreateAPIView):
    serializer_class = CompanySerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return user.userprofile.linked_companies.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)            

class Templates(generics.ListCreateAPIView):
    serializer_class = TemplateSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Template.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TemplatesCreate(APIView):    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            template_post = request.data
            if 'code' in request.data:
                template = Template(type_of_company=template_post['type_of_company'],description=template_post['description'], code=template_post['code'])
            else:
                template = Template(type_of_company=template_post['type_of_company'],description=template_post['description'])
            
            template.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TemplateDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TemplateSerializer
    

    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Template.objects.all()
        else:
            return ""

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            template = Template.objects.get(id=self.kwargs['pk'])
            template.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class Stages(generics.ListCreateAPIView):
    serializer_class = StageSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            template = Template.objects.get(id=self.kwargs['pk']) 
            return StageTemplate.objects.filter(template=template)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class AllStages(generics.ListCreateAPIView):
    serializer_class = StageSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):            
            return StageTemplate.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class StageCreate(APIView):    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            stage_post = request.data
            level = Level.objects.get(id=stage_post['level'])
            template = Template.objects.get(id=stage_post['template'])
            
            stage = StageTemplate(name=stage_post['name'],description=stage_post['description'], level=level,template=template)
            if 'externalFilesFolder' in request.data:
                stage.externalFilesFolder = request.data['externalFilesFolder']
            stage.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class StageDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = StageSerializer
    

    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return StageTemplate.objects.all()
        else:
            return ""

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            stage = StageTemplate.objects.get(id=self.kwargs['pk'])
            stage.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            stage_post = request.data
            level = Level.objects.get(id=stage_post['level'])
            template = Template.objects.get(id=stage_post['template'])

            stage = StageTemplate.objects.get(id=stage_post['id'])

            stage.name = stage_post['name']
            stage.description = stage_post['description']
            if 'externalFilesFolder' in request.data:
                stage.externalFilesFolder = request.data['externalFilesFolder']
            stage.level = level
            stage.template = template
            stage.save()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class StageRead(generics.RetrieveAPIView):
    queryset = StageTemplate.objects.all()
    serializer_class = StageSerializer

class Tasks(generics.ListCreateAPIView):
    serializer_class = TaskSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            stage = StageTemplate.objects.get(id=self.kwargs['pk']) 
            return TaskTemplate.objects.filter(stage=stage)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TaskCreate(APIView):    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            task_post = request.data
            stage = StageTemplate.objects.get(id=task_post['stage'])
            
            task = TaskTemplate(name=task_post['name'],description=task_post['description'], stage=stage)
            task.save()

            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)


class TaskDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskSerializer
    

    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return TaskTemplate.objects.all()
        else:
            return ""

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            task = TaskTemplate.objects.get(id=self.kwargs['pk'])
            task.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            task_post = request.data
            stage = StageTemplate.objects.get(id=task_post['stage'])

            task = TaskTemplate.objects.get(id=task_post['id'])

            task.name = task_post['name']
            task.description = task_post['description']
            task.stage = stage
            task.save()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class AllTasks(generics.ListCreateAPIView):
    serializer_class = TaskSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):            
            return TaskTemplate.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class Levels(generics.ListCreateAPIView):
    serializer_class = LevelSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Level.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class LevelDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LevelSerializer
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Level.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            level = Level.objects.get(id=self.kwargs['pk'])
            level.delete()
            f = open(level.imageFileSystem, 'w')
            f.close()

            if os.path.isfile(level.image):
                os.remove(f.name)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            
            level = Level.objects.get(id=pk)
            

            
            if (len(request.FILES) != 0):         
                level.name=request.data['level[name]']
                level.description=request.data['level[description]']
                level.save()


                my_file = request.FILES['file']
                if level.imageFileSystem!=None:
                    f = open(level.imageFileSystem, 'w')
                    f.close()
                if level.image!=None:
                    if os.path.isfile(level.image):
                        os.remove(f.name)

                filename = Globals.baseURLFileSystem()+'dist/img/levels/'+str(level.id)
                with open(filename, 'wb+') as temp_file:
                    for chunk in my_file.chunks():
                        temp_file.write(chunk)

                level.image = Globals.baseURL()+'dist/img/levels/'+str(level.id)
                level.imageFileSystem = Globals.baseURLFileSystem()+'dist/img/levels/'+str(level.id)
                level.save()
            else:
                 
               
                level.name=request.data['name']
                level.description=request.data['description']
                level.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)
        
        



class LevelCreate(APIView):
    parser_classes = (MultiPartParser, FormParser,)
    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            level_post = request.data
            level = Level(name=request.data['level[name]'],description=request.data['level[description]'])
            level.save()
            my_file = request.FILES['file']
            filename = Globals.baseURLFileSystem()+'dist/img/levels/'+str(level.id)
            with open(filename, 'wb+') as temp_file:
                for chunk in my_file.chunks():
                    temp_file.write(chunk)

            level.image = Globals.baseURL()+'dist/img/levels/'+str(level.id)
            level.imageFileSystem = Globals.baseURLFileSystem()+'dist/img/levels/'+str(level.id)
            level.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class LevelRead(generics.RetrieveAPIView):
    queryset = Level.objects.all()
    serializer_class = LevelSerializer
        
class Challenges(generics.ListCreateAPIView):
    serializer_class = ChallengeSerializer
    
    def get_queryset(self):
        user = self.request.user
        return Challenge.objects.filter(in_charge=user)
        

class ChallengesSent(generics.ListCreateAPIView):
    serializer_class = ChallengeSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return Challenge.objects.filter(sender=user)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)            

class ChallengesCreate(APIView):    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            challenge_post = request.data
            badge = Badge.objects.get(id=request.data['badge_id'])
            in_charge = User.objects.get(username=request.data['in_charge_username'])
            challenge = Challenge(name=challenge_post['name'],description=challenge_post['description'], badge = badge, in_charge = in_charge, sender = user, ending_date=challenge_post['ending_date'])
            
            challenge.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ChallengeDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ChallengeSerializer
    

    def get_queryset(self):
        user = self.request.user
        return Challenge.objects.all()
        

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            challenge = Challenge.objects.get(id=self.kwargs['pk'])
            challenge.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            
            challenge = Challenge.objects.get(id=pk)
            

            
            if (len(request.FILES) != 0):   

                challenge.delivery_text=request.data['task_story[delivery_text]']
                challenge.sent = True
               
                

                my_file = request.FILES['file']
                if challenge.deliveryFileSystem!=None:
                    f = open(challenge.deliveryFileSystem, 'w')
                    f.close()
                if challenge.delivery_file!=None:
                    if os.path.isfile(challenge.delivery_file):
                        os.remove(f.name)

                filename = Globals.baseURLFileSystem()+'dist/files/delivery_files_challenge/'+str(challenge.id)
                with open(filename, 'wb+') as temp_file:
                    for chunk in my_file.chunks():
                        temp_file.write(chunk)

                challenge.delivery_file = Globals.baseURL()+'dist/files/delivery_files_challenge/'+str(challenge.id)
                challenge.deliveryFileSystem = Globals.baseURLFileSystem()+'dist/files/delivery_files_challenge/'+str(challenge.id)
                
                if (challenge.in_charge == user or PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
                    challenge.save()
                
            else:
                
                if 'delivery_text' in request.data:
                    challenge.sent = True               
                    challenge.delivery_text = request.data['delivery_text']
                    
                    
                
                
                if (challenge.in_charge == user or challenge.creator == user or PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
                    challenge.save()
                
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ChallengeReview(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ChallengeSerializer
    

    def put(self, request, pk):
        user = self.request.user
            
        challenge = Challenge.objects.get(id=pk)
        
        if (challenge.sender == user):
            if 'comments' in request.data:
                challenge.comments = request.data['comments']
                
            if 'ready' in request.data:
                if (request.data['ready']==True):
                    challenge.sent = False
                    challenge.accepted = True                    
                    usp = challenge.in_charge.userprofile
                    usp.badges.add(challenge.badge)
                    if (usp.company):
                        usp.company.badges.add(challenge.badge)
                    usp.save()

            else:
                challenge.sent = False
            
            
            challenge.save()
        
        return Response(status=status.HTTP_201_CREATED)

class Badges(generics.ListCreateAPIView):
    serializer_class = BadgeSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return Badge.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class BadgesUserCompany(APIView):
    
    
    def get(self, request,pk):
        user = self.request.user
        company = Company.objects.get(id=pk)
        if (user.userprofile.company == company or PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            user_badges = {}
            company_badges = {}
            if company.badges.all():
                company_badges = company.badges.all()
            if user.userprofile.badges.all():
                user_badges = user.userprofile.badges.all()
            
            serializer_context = {'request': Request(request),}   
            company_badges_s = BadgeSerializer(company_badges, many=True, context=serializer_context)
            user_badges_s = BadgeSerializer(user_badges, many=True, context=serializer_context)
            return Response({'company_badges': company_badges_s.data, 'user_badges': user_badges_s.data}, status=status.HTTP_200_OK)


                
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)


class BadgeDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = BadgeSerializer
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return Badge.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            badge = Badge.objects.get(id=self.kwargs['pk'])
            badge.delete()
            f = open(badge.imageFileSystem, 'w')
            f.close()

            if os.path.isfile(badge.image):
                os.remove(f.name)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            
            badge = Badge.objects.get(id=pk)
            

            
            if (len(request.FILES) != 0):         
                badge.name=request.data['badge[name]']
                badge.description=request.data['badge[description]']
                badge.save()


                my_file = request.FILES['file']
                if badge.imageFileSystem!=None:
                    f = open(badge.imageFileSystem, 'w')
                    f.close()
                if badge.image!=None:
                    if os.path.isfile(badge.image):
                        os.remove(f.name)

                filename = Globals.baseURLFileSystem()+'dist/img/badges/'+str(badge.id)
                with open(filename, 'wb+') as temp_file:
                    for chunk in my_file.chunks():
                        temp_file.write(chunk)

                badge.image = Globals.baseURL()+'dist/img/badges/'+str(badge.id)
                badge.imageFileSystem = Globals.baseURLFileSystem()+'dist/img/badges/'+str(badge.id)
                badge.save()
            else:
                 
               
                badge.name=request.data['name']
                badge.description=request.data['description']
                badge.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)
        
        



class BadgesCreate(APIView):
    parser_classes = (MultiPartParser, FormParser,)
    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            badge_post = request.data
            badge = Badge(name=request.data['badge[name]'],description=request.data['badge[description]'])
            badge.save()
            my_file = request.FILES['file']
            filename = Globals.baseURLFileSystem()+'dist/img/badges/'+str(badge.id)
            with open(filename, 'wb+') as temp_file:
                for chunk in my_file.chunks():
                    temp_file.write(chunk)

            badge.image = Globals.baseURL()+'dist/img/levels/'+str(badge.id)
            badge.imageFileSystem = Globals.baseURLFileSystem()+'dist/img/badges/'+str(badge.id)
            badge.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)


class TaskStoryDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskStorySerializer
    def get_queryset(self):
        user = self.request.user
        task_story = TaskStory.objects.get(id=self.kwargs['pk'])
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            if (PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user)):
                if (task_story.company == user.userprofile.company or task_story.in_charge == user or task_story.creator == user):
                    return TaskStory.objects.all()
            else:
                return TaskStory.objects.all()
            
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            task_story = TaskStory.objects.get(id=self.kwargs['pk'])

            if (PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user)):
                if (task_story.company == user.userprofile.company or task_story.in_charge == user):
                    task_story.delete()
                    f = open(task_story.deliveryFileSystem, 'w')
                    f.close()
            else:
                task_story.delete()
                f = open(task_story.deliveryFileSystem, 'w')
                f.close()
                            

            if os.path.isfile(task_story.delivery_file):
                os.remove(f.name)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            
            task_story = TaskStory.objects.get(id=pk)
            

            
            if (len(request.FILES) != 0):   

                task_story.delivery_text=request.data['task_story[delivery_text]']
                task_story.sent = True
                
                if (task_story.company == user.userprofile.company or task_story.in_charge == user or task_story.creator == user or PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
                    task_story.save()
            

                #pending_task = Pending(task_story=task_story)
                #pending_task.save()


                my_file = request.FILES['file']
                if task_story.deliveryFileSystem!=None:
                    f = open(task_story.deliveryFileSystem, 'w')
                    f.close()
                if task_story.delivery_file!=None:
                    if os.path.isfile(task_story.delivery_file):
                        os.remove(f.name)

                filename = Globals.baseURLFileSystem()+'dist/files/delivery_files/'+str(task_story.id)
                with open(filename, 'wb+') as temp_file:
                    for chunk in my_file.chunks():
                        temp_file.write(chunk)

                task_story.delivery_file = Globals.baseURL()+'dist/files/delivery_files/'+str(task_story.id)
                task_story.deliveryFileSystem = Globals.baseURLFileSystem()+'dist/files/delivery_files/'+str(task_story.id)
                
                if (task_story.company == user.userprofile.company or task_story.in_charge == user or task_story.creator == user or PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
                    task_story.save()
        
            else:
                #User sent the task:
                if 'delivery_text' in request.data:
                    task_story.sent = True               
                    task_story.delivery_text = request.data['delivery_text']
                    task_story.save()
                    #pending_task = Pending(task_story=task_story)
                    #pending_task.save()
                #The task is updated:
                else:
                    if 'in_charge_username' in request.data and request.data['in_charge_username']!= "":
                        in_charge = User.objects.get(username=request.data['in_charge_username'])
                        task_story.in_charge= in_charge
                    if 'start_date' in request.data:
                        task_story.start_date= request.data['start_date']
                    if 'ending_date' in request.data:
                        task_story.ending_date= request.data['ending_date'] 
                    if 'points' in request.data:
                        task_story.points= request.data['points'] 
                    if 'task_area_id' in request.data:       
                        taskArea = TaskArea.objects.get(id=request.data['task_area_id'] )                 
                        task_story.area = taskArea
                    if 'status_id' in request.data:       
                        statusObj = Status.objects.get(id=request.data['status_id'] )                 
                        task_story.status = statusObj
                    
                    if not task_story.creator: 
                        task_story.creator = user
                    
                    
                
                if (task_story.company == user.userprofile.company or task_story.in_charge == user or task_story.creator == user or PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
                    task_story.save()
                
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TaskStoryReview(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskStorySerializer
    

    def put(self, request, pk):
        user = self.request.user
            
        task_story = TaskStory.objects.get(id=pk)
        
        if (task_story.creator == user):
            if 'comments' in request.data:
                task_story.comments = request.data['comments']
                
            if 'ready' in request.data:
                if (request.data['ready']==True):
                    task_story.sent = False
                    task_story.accepted = True
            else:
                task_story.sent = False
            
            
            task_story.save()
        
        return Response(status=status.HTTP_201_CREATED)
        

class TaskStoriesStageCompany(generics.ListCreateAPIView):
    serializer_class = TaskStorySerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user)):
            stage = StageTemplate.objects.get(id=self.kwargs['cy']) 
            company = Company.objects.get(id=self.kwargs['co']) 
            tt = TaskTemplate.objects.filter(stage=stage)
            taskStories = []
            for taskTemplate in tt:
                for taskStory in taskTemplate.task_template.filter(company=company):
                    taskStories.append(taskStory)
            return taskStories
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TaskStoriesUser(generics.ListCreateAPIView):
    serializer_class = TaskStorySerializer
    
    def get_queryset(self):
        user = self.request.user
        
        userNew = User.objects.get(id=self.kwargs['us']) 
        stage = StageTemplate.objects.get(id=self.kwargs['st']) 
        if (userNew==user):
            return TaskStory.objects.filter(in_charge=userNew, story_have_not_template=True, stage_not_template=stage)
            
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TaskStoriesIncubatorEmployee(generics.ListCreateAPIView):
    serializer_class = TaskStorySerializer
    
    def get_queryset(self):
        user = self.request.user
        
        userNew = User.objects.get(id=self.kwargs['us']) 
        
        if (userNew==user):
            return TaskStory.objects.filter(in_charge=userNew, story_have_not_template=True)
            
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TaskStoryCreate(generics.ListCreateAPIView):
    serializer_class = TaskStorySerializer

    def post(self, request):
        user = self.request.user        
            
        task_story = TaskStory()

        if 'in_charge_username' in request.data and request.data['in_charge_username']!= "":
            in_charge = User.objects.get(username=request.data['in_charge_username'])
            task_story.in_charge = in_charge
        if 'start_date' in request.data:
            task_story.start_date= request.data['start_date']
        if 'ending_date' in request.data:
            task_story.ending_date= request.data['ending_date'] 
        if 'points' in request.data:
            task_story.points= request.data['points'] 
        if 'task_area_id' in request.data:       
            taskArea = TaskArea.objects.get(id=request.data['task_area_id'] )                 
            task_story.area = taskArea
        if 'status_id' in request.data:       
            statusObj = Status.objects.get(id=request.data['status_id'] )                 
            task_story.status = statusObj

        task_story.stage_not_template = StageTemplate.objects.get(id=request.data['stage_id'])
        if (in_charge.userprofile.company):
            task_story.company = in_charge.userprofile.company
        task_story.story_have_not_template = True
        task_story.name_not_template = request.data['name_not_template'] 
        task_story.description_not_template = request.data['description_not_template'] 
        
        
        task_story.creator = user
            
        
        task_story.save()
        return Response(status=status.HTTP_201_CREATED)
    


class PendingAll(generics.ListCreateAPIView):
    serializer_class = TaskStorySerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return user.task_creator.filter(sent=True)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class PendingChallenges(generics.ListCreateAPIView):
    serializer_class = ChallengeSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return user.sender_challenge.filter(sent=True)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)            


class FilesManagement(APIView):    
    renderer_classes = (JSONRenderer, )

    def timestamp_to_str(self, timestamp, format_str='%Y-%m-%d %I:%M:%S'):
        date = datetime.datetime.fromtimestamp(timestamp)
        return date.strftime(format_str)

    def filemode(self, mode):
        is_dir = 'd' if stat.S_ISDIR(mode) else '-'
        dic = {'7': 'rwx', '6': 'rw-', '5': 'r-x', '4': 'r--', '0': '---'}
        perm = str(oct(mode)[-3:])
        return is_dir + ''.join(dic.get(x, x) for x in perm)

    def get_file_information(self, path):
        fstat = os.stat(path)
        if stat.S_ISDIR(fstat.st_mode):
            ftype = 'dir'
        else:
            ftype = 'file'

        fsize = fstat.st_size
        ftime = self.timestamp_to_str(fstat.st_mtime)
        fmode = self.filemode(fstat.st_mode)

        return ftype, fsize, ftime, fmode

    def post(self, request, format=None):
        user = self.request.user
        self.show_dotfiles = True
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            action = request.data['action']
            
            if action == "list":
                path = os.path.abspath(Globals.baseURLFileSystem()[:-1] + request.data['path'])
                if not os.path.exists(path) or not path.startswith(Globals.baseURLFileSystem()[:-1]):
                    Response({'result': ''}, status=status.HTTP_200_OK)

                files = []
               
                for fname in sorted(os.listdir(path)):
                    if fname.startswith('.') and not self.show_dotfiles:
                        continue
                    
                    fpath = os.path.join(path, fname)

                    try:
                        ftype, fsize, ftime, fmode = self.get_file_information(fpath)
                        
                    except Exception as e:
                        continue
                    
                    files.append({
                        'name': fname,
                        'rights': fmode,
                        'size': fsize,
                        'date': ftime,
                        'type': ftype,
                    })

                return Response({'result': files}, status=status.HTTP_200_OK)

            elif action == "getContent":

                try:
                    path = os.path.abspath(Globals.baseURLFileSystem()[:-1] + request.data['item'])
                    if not path.startswith(Globals.baseURLFileSystem()[:-1]):
                        return Response({'result': {'success': 'false', 'error': 'Invalid path'}})

                    with open(path, 'r', encoding="utf-8") as f:
                        content = f.read()
                except Exception as e:
                    content = "Exception"

                return Response({'result': content}, status=status.HTTP_200_OK)

            elif action == "rename":

                try:
                    src = os.path.abspath(Globals.baseURLFileSystem()[:-1] + request.data['item'])
                    dst = os.path.abspath(Globals.baseURLFileSystem()[:-1] + request.data['newItemPath'])
                    print('rename {} {}'.format(src, dst))
                    if not (os.path.exists(src) and src.startswith(Globals.baseURLFileSystem()[:-1]) and dst.startswith(Globals.baseURLFileSystem()[:-1])):
                        return Response({'result': {'success': 'false', 'error': 'Invalid path'}})

                    shutil.move(src, dst)
                except Exception as e:
                    return Response({'result': {'success': 'false', 'error': e.message}})

                return Response({'result': {'success': 'true', 'error': ''}})

            elif action == "copy":
                
                items = request.data['items']
                if len(items) == 1 and 'singleFilename' in request.data:
                    src = os.path.abspath(Globals.baseURLFileSystem() + items[0])
                    dst = os.path.abspath(Globals.baseURLFileSystem() + request.data['singleFilename'])
                    if not (os.path.exists(src) and src.startswith(Globals.baseURLFileSystem()[:-1]) and dst.startswith(Globals.baseURLFileSystem()[:-1])):
                        return Response({'result': {'success': 'false', 'error': 'File not found'}})

                    shutil.move(src, dst)
                else:
                    path = os.path.abspath(Globals.baseURLFileSystem()[:-1] + request.data['newPath'])
                    for item in items:
                        src = os.path.abspath(Globals.baseURLFileSystem()[:-1] + item)
                        if not (os.path.exists(src) and src.startswith(Globals.baseURLFileSystem()[:-1]) and path.startswith(Globals.baseURLFileSystem()[:-1])):
                            return Response({'result': {'success': 'false', 'error': 'Invalid path'}})

                        shutil.move(src, dst)

                        
                

                return Response({'result': {'success': 'true', 'error': ''}})
                
            

            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TaskAreas(generics.ListCreateAPIView):
    serializer_class = TaskAreaSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return TaskArea.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class StatusView(generics.ListCreateAPIView):
    serializer_class = StatusSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsCompanyBoard",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return Status.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ScreenshotCompanyView(generics.ListCreateAPIView):
    serializer_class = ScreenshotCompanySerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            company = Company.objects.get(id=self.kwargs['pk'])
            return ScreenshotCompany.objects.filter(company=company)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ScreenshotCompanyCreate(APIView):    
    def post(self, request, pk, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            screenshot_post = request.data       
            company = Company.objects.get(id=self.kwargs['pk'])
            if 'graduated' in request.data:
                if (screenshot_post['graduated']==True):
                    company.graduated = True 
                    company.save()
            screenshot = ScreenshotCompany(company=company,employees_number= None or screenshot_post['employees_number'], number_of_stages_approved = None or screenshot_post['approved_stages'], employees_salary = None or screenshot_post['employees_salary'])
            
            screenshot.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ScreenshotCompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ScreenshotCompanySerializer
    

    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user) or PermissionsManagement.checkUserPermission("UserIsMentor",user)):
            return ScreenshotCompany.objects.all()
        else:
            return ""

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            screenshot = ScreenshotCompany.objects.get(id=self.kwargs['pk'])
            screenshot.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ScreenshotIncubatorView(generics.ListCreateAPIView):
    serializer_class = ScreenshotIncubatorSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return ScreenshotIncubator.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ScreenshotIncubatorCreate(APIView):    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            screenshot_post = request.data       
            screenshot = ScreenshotIncubator(companies_number= None or screenshot_post['companies_number'], earnings= None or screenshot_post['earnings'])
            
            screenshot.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class ScreenshotIncubatorDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ScreenshotIncubatorSerializer
    

    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return ScreenshotIncubator.objects.all()
        else:
            return ""

    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            screenshot = ScreenshotIncubator.objects.get(id=self.kwargs['pk'])
            screenshot.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class RoomListing(generics.ListCreateAPIView):
    serializer_class = RoomsSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) ):
            
            return Room.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class RoomCreate(APIView):    
    def post(self, request, format=None):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
            room_post = request.data       
        
            room = Room(name=room_post['name'], createdBy=user)
            
            room.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class RoomDelete(APIView):    
   


    def delete(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user)):
            room = Room.objects.get(id=self.kwargs['pk'])
            room.delete()
          
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)




class PermissionGroups(generics.ListCreateAPIView):
    serializer_class = PermissionGroupSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            return PermissionGroup.objects.all()
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)            

class UserProfiles(generics.ListCreateAPIView):
    serializer_class = UserProfileSerializer
    
    def get_queryset(self):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            # We create all the UserProfiles for the Users that don't have one
            users = User.objects.all()
            for us in users:                
                try:
                    u = us.userprofile                        
                except ObjectDoesNotExist:
                    up = UserProfile(user=us)
                    up.save()
        
            return UserProfile.objects.all()


            
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class UserProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    def put(self, request, pk):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            template_post = request.data
            userProfile = UserProfile.objects.get(id=pk)  
            if 'permissionGroupId' in request.data:
                pg = PermissionGroup.objects.get(id=request.data['permissionGroupId'])
                usp = UserProfile.objects.get(id=request.data['userProfileId'])
                usp.permissionGroup = pg

                #Change to customize
                if (pg.name=='JD Empresa'):
                    comp = Company.objects.get(id=request.data['companyId'])
                    usp.company = comp
                    usp.is_in_board = True
                if (pg.name=='Mentores'):
                    usp.linked_companies.clear()
                    linked_companies = request.data['companiesId']
                    for comp in linked_companies:
                        company = Company.objects.get(id=comp['company_id'])
                        usp.linked_companies.add(company)

                
                usp.save()

                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)



class GetUserProfile(generics.ListCreateAPIView):
    serializer_class = UserProfileSerializer
    def get_queryset(self):
        
        thisUser = User.objects.get(id=self.kwargs['pk'])  
        
        return UserProfile.objects.filter(user=thisUser)  
        
class NumberGraduatedCompanies(APIView):
    
    
    def get(self, request):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            
            return Response({'result': Company.objects.filter(graduated=True).count()}, status=status.HTTP_200_OK)
     
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TotalNumberEmployees(APIView):
    
    
    def get(self, request):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            employees = []            
            for company in Company.objects.all():
                #We look for the last Screenshot Company to know the current number of employees
                sc = ScreenshotCompany.objects.filter(company=company).order_by('-id')
                if (sc):
                    employees.append(sc[0].employees_number or 0)

            count = 0
            for number in employees:
                count = count + number

            return Response({'result': count}, status=status.HTTP_200_OK)
     
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class TotalSalary(APIView):
    
    
    def get(self, request):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            employees = []            
            for company in Company.objects.all():
                #We look for the last Screenshot Company to know the current number of employees
                sc = ScreenshotCompany.objects.filter(company=company).order_by('-id')
                if (sc):
                    employees.append(sc[0].employees_salary or 0)

            count = 0
            for number in employees:
                count = count + number

            return Response({'result': count}, status=status.HTTP_200_OK)
     
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)

class IncubatorEarnings(APIView):
    
    
    def get(self, request):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            si = ScreenshotIncubator.objects.all().order_by('-id')
            earnings = 0
            if (si):
                earnings = si[0].earnings

            return Response({'result': earnings}, status=status.HTTP_200_OK)
     
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)   


class CompanyPoints(APIView):
    
    
    def get(self, request, pk):
        user = self.request.user
        
        company = Company.objects.get(id=pk)
        points = 0
        for task_story in company.company_task_story.all():
            if task_story.points != None and task_story.accepted == True:
                points = points + task_story.points

        return Response({'results': points}, status=status.HTTP_200_OK)
     
        

class Logs(APIView):
    
    
    def get(self, request):
        user = self.request.user
        if (PermissionsManagement.checkUserPermission("UserIsAdmin",user) or PermissionsManagement.checkUserPermission("UserIsManager",user)):
            
            serializer_context = {'request': Request(request),}
            task_stories = TaskStorySerializer(TaskStory.objects.all().order_by('-sent_date'), many=True, context=serializer_context)
            return Response({'results': task_stories.data}, status=status.HTTP_200_OK)
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)  



#************** CHAT VIEWS *****************#


def login(request, **kwargs):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)
    else:
        return contrib_login(request, **kwargs)

class LoginRequired(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequired, self).dispatch(*args,**kwargs)


class AuthView(object):
    permission_classes = (IsAuthenticated, )


class RoomView(AuthView, views.APIView):
    def message_tree(self, qs):
        for msg in qs:
            ser = RoomSerializer(msg)
            data = ser.data
            data['children'] = self.message_tree(msg.responses.all())
            yield data

    def get(self, request, format=None, id=1, page="1"):
        room = get_object_or_404(Room, pk=int(id))
        page = int(page)
        queryset = Message.objects.filter(room=room, responseTo=None).order_by('-id')
        paginator = Paginator(queryset, 10, allow_empty_first_page=True)
        try:
            roots = paginator.page(page)
        except EmptyPage:
            roots = []
        data = self.message_tree(roots)
        return Response(data)


class UserView(AuthView, views.APIView):
    def get(self, request, format=None):
        data = UserSerializerChat(request.user).data
        return Response(data)


class LobbyView(AuthView, views.APIView):
    def populate(self, qs):
        for room in qs:
            ser = LobbySerializer(room)
            data = ser.data
            newest = MessageSerializer(room.message_set.order_by('-created')[0:5], many=True)
            data['newest'] = newest.data
            yield data

    def get(self, request, format=None):
        result = []
        data = self.populate(Room.objects.all())
        return Response(data)


class MessageView(AuthView, views.APIView):
    def post(self, request, format=None):
        data = request.data
        if data.get('id'):
            #no modification of posts allowed
            return HttpResponseForbidden()
        ser = MessageSerializer(data=data, partial=True)
        if ser.is_valid():
            room = Room.objects.get(id=request.data['room'])
            serM = Message(content=request.data['content'], writer=request.user, room= room)
            serM.save()
            
        return Response(ser.data)


class MainView(AuthView, views.APIView):
    def get(self, request, format=None):
        ser = RoomsSerializer(Room.objects.all(), many=True)
        return Response(ser.data)


class AccountView(LoginRequired, FormView):
    template_name="user-settings.html"
    form_class = AccountForm
    success_url = '/chat/index.html'
    
    def get_context_data(self, **kwargs):
        context = super(AccountView, self).get_context_data(**kwargs)
        try:
            context['img'] = self.request.user.userextra.img
        except:
            pass
        return context

    def form_valid(self, form):
        ret = super(AccountView, self).form_valid(form)
        if ret.status_code == 302:
            if not form.instance.pk:
                form.instance.user = self.request.user
            form.save()
        return ret

#************** CHAT VIEWS ************#