import sys, os
cwd = os.getcwd()
sys.path.append(cwd)
project_location = cwd + '/ptsproject'
sys.path.insert(0,project_location)

INTERP = os.path.expanduser("/opt/py34/bin/python")

if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)

sys.path.insert(0,'/opt/py34/bin')

sys.path.insert(0,'/opt/py34/lib/python3.4/site-packages')


os.environ['DJANGO_SETTINGS_MODULE'] = "ptsproject.settings"
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

