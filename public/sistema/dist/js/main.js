var mainApp = angular.module('main', ['ngCookies','ngRoute','ngFileUpload','angucomplete']);

/*Routes management*/
mainApp.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);

mainApp.config(function($routeProvider) {
    $routeProvider
    .when("/", {
         redirectTo: '/tablero'
    })
    .when("/tablero", {
        templateUrl : "views/dashboard/dashboard.html",
        controller: 'Dashboard',
    })
    .when("/empresas", {
        templateUrl : "views/companies/companies-principal.html",
        controller: 'Companies',
    })
    .when("/crear_empresa", {
        templateUrl : "views/companies/companies-create.html",
        controller: 'CompaniesCreate',
    })
    .when("/editar_empresa/:companyId?", {
        templateUrl : "views/companies/company-edit.html",
        controller: 'CompanyEdit',
    })
    .when("/personalizar_tareas/:companyId?", {
        templateUrl : "views/companies/tasks-customization.html",
        controller: 'TasksCustomization',
    })
    .when("/usuarios", {
        templateUrl : "views/users/users.html",
        controller: 'AllUsers',
    })
    .when("/plantillas", {
        templateUrl : "views/templates/templates-principal.html",
        controller: 'Templates',
    })
    .when("/crear_plantilla", {
        templateUrl : "views/templates/templates-create.html",
        controller: 'TemplatesCreate',
    })
    .when("/editar_plantilla/:templateId?", {
        templateUrl : "views/templates/templates-edit.html",
        controller: 'TemplatesEdit',
    })
    .when("/etapas/:templateId?", {
        templateUrl : "views/stages/stages-principal.html",
        controller: 'Stages',
    })
    .when("/editar_etapa/:stageId?", {
        templateUrl : "views/stages/stage-edit.html",
        controller: 'StageEdit',
    })
    .when("/crear_etapa/:templateId?", {
        templateUrl : "views/stages/stages-create.html",
        controller: 'StagesCreate',
    })
    .when("/niveles", {
        templateUrl : "views/levels/levels-principal.html",
        controller: 'Levels',
    })
    .when("/crear_nivel", {
        templateUrl : "views/levels/levels-create.html",
        controller: 'LevelsCreate',
    })
    .when("/editar_nivel/:levelId?", {
        templateUrl : "views/levels/level-edit.html",
        controller: 'LevelEdit',
    })
    .when("/tareas/:stageId?", {
        templateUrl : "views/tasks/tasks-principal.html",
        controller: 'Tasks',
    })
    .when("/crear_tarea/:stageId?", {
        templateUrl : "views/tasks/tasks-create.html",
        controller: 'TasksCreate',
    })
    .when("/crear_tarea_usuario", {
        templateUrl : "views/tasks/tasks-create-user.html",
        controller: 'TasksCreateCompanyUser',
    })
    .when("/editar_tarea/:taskId?", {
        templateUrl : "views/tasks/task-edit.html",
        controller: 'TaskEdit',
    })
    .when("/revisar_tarea/:pendingObjectId?", {
        templateUrl : "views/tasks/task-review.html",
        controller: 'TaskReview',
    })
    .when("/revisar_desafio/:pendingObjectId?", {
        templateUrl : "views/challenges/challenge-review.html",
        controller: 'ChallengeReview',
    })
    .when("/inicio_empresa", {
        templateUrl : "views/company_user/company_user_home.html",
        controller: 'CompanyUserHome',
    })
    .when("/completar_tarea/:taskId?", {
        templateUrl : "views/company_user/complete-task.html",
        controller: 'CompleteTask',
    })
    .when("/completar_desafio/:challengeId?", {
        templateUrl : "views/company_user/complete-challenge.html",
        controller: 'CompleteChallenge',
    })
    .when("/desafios", {
        templateUrl : "views/challenges/challenges-principal.html",
        controller: 'Challenges',
    })
    .when("/crear_desafio", {
        templateUrl : "views/challenges/challenges-create.html",
        controller: 'ChallengeCreate',
    })
    .when("/editar_desafio/:challengeId?", {
        templateUrl : "views/challenges/challenge-edit.html",
        controller: 'ChallengeEdit',
    })
    .when("/insignias", {
        templateUrl : "views/badges/badges-principal.html",
        controller: 'Badges',
    })
    .when("/crear_insignia", {
        templateUrl : "views/badges/badges-create.html",
        controller: 'BadgesCreate',
    })
    .when("/editar_insignia/:badgeId?", {
        templateUrl : "views/badges/badge-edit.html",
        controller: 'BadgeEdit',
    })
    .when("/pendientes", {
        templateUrl : "views/pending/pending-principal.html",
        controller: 'Pending',
    })
    .when("/estados/:companyId?", {
        templateUrl : "views/screenshots/screenshots-companies-principal.html",
        controller: 'ScreenshotsCompany',
    })
    .when("/crear_estado_empresa/:companyId?", {
        templateUrl : "views/screenshots/screenshots-companies-create.html",
        controller: 'ScreenshotsCompanyCreate',
    })
    .when("/ver_estado_empresa/:screenshotId?", {
        templateUrl : "views/screenshots/screenshot-company-detail.html",
        controller: 'ScreenshotCompanyDetail',
    })
    .when("/estados_incubadora", {
        templateUrl : "views/screenshots/screenshots-incubator-principal.html",
        controller: 'ScreenshotsIncubator',
    })
    .when("/crear_estado_incubadora", {
        templateUrl : "views/screenshots/screenshots-incubator-create.html",
        controller: 'ScreenshotsIncubatorCreate',
    })
    .when("/ver_estado_incubadora/:screenshotId?", {
        templateUrl : "views/screenshots/screenshots-incubator-detail.html",
        controller: 'ScreenshotIncubatorDetail',
    })
    .when("/resumen_estados_incubadora", {
        templateUrl : "views/screenshots/screenshots-summary.html",
        controller: 'ScreenshotSummary',
    })
    .when("/resumen_estados_empresa/:companyId?", {
        templateUrl : "views/screenshots/screenshots-summary-company.html",
        controller: 'CompanyScreenshotSummary',
    })
    .when("/tablero_empresa", {
        templateUrl : "views/dashboard/company_dashboard.html",
        controller: 'CompanyDashboard',
    })
    .when("/historias", {
        templateUrl : "views/logs/logs-principal.html",
        controller: 'Logs',
    })
    .when("/salas", {
        templateUrl : "views/rooms/rooms.html",
        controller: 'Rooms',
    })
    .when("/crear_sala", {
        templateUrl : "views/rooms/rooms-create.html",
        controller: 'RoomsCreate',
    })
    
    ;

});

mainApp.run(function ($rootScope) {
  $rootScope.loginShow = true;
  $rootScope.loading = false;
  $rootScope.userID = "";
  $rootScope.page_title = "Dashboard";
  $rootScope.main_message = "";
  /*Frontend Base URL:*/
  $rootScope.baseURL = "http://pts.hezionstudios.com/sistema/";
  /*Backend Base URL:*/
  $rootScope.baseURLBackend = "http://pts.hezionstudios.com/";  
});



/*Dashboard*/
mainApp.controller('Dashboard', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  if ($rootScope.loginShow != true){
    //$rootScope.loading = true;  
  }
  
  var token = $cookies.get('tokenpts');
  $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
        $rootScope.loginShow = false;
        $scope.authShow = true;
        
        //Calculating User ID:
        $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $rootScope.userID = response.data['pk']; 
        
          //Calculating User Profile
          $http({url: $rootScope.baseURLBackend+'get-user-profile/'+$rootScope.userID+'/', method: 'GET', headers: {
          'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            
            
          

               
            $http({url: $rootScope.baseURLBackend+'task-stories-incubator-employee/'+$rootScope.userID+'/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {                   
              
              task_stories = response.data['results'];
              var task_stories_pending = [];
              $scope.task_stories = {};
              task_stories.forEach(function(entry) {
                if (!entry.sent && !entry.accepted){
                  task_stories_pending.push(entry)
                  
                }
              
              });
              
              $scope.taskstories = task_stories_pending;          
            });

            $http({url: $rootScope.baseURLBackend+'challenges/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {
              challenges = response.data['results'];
              var challenges_pending = [];
              $scope.task_stories = {};
              challenges.forEach(function(entry) {
                if (!entry.sent && !entry.accepted){
                  challenges_pending.push(entry)
                  
                }
              
              });
              
              $scope.challenges = challenges_pending;
            });
            
            //User View Management
            //Change to customize

            if ($rootScope.userProfile.permissionGroup.name == 'JD Empresa'){
              $rootScope.showAdmin = false;

              $location.path( "/tablero_empresa" );

            } else if ($rootScope.userProfile.permissionGroup.name == 'Administradores' || $rootScope.userProfile.permissionGroup.name == 'Gerentes'){
              $rootScope.loading = false;
              $rootScope.showAdmin = true;
              $http({url: $rootScope.baseURLBackend+'users/', method: 'GET', headers: {
                'Authorization': 'Token '+token} }).then(
                function successCallback(response) {
                
                $scope.usersNumber = response.data['count']; 
              }, function errorCallback(response) {});

              $http({url: $rootScope.baseURLBackend+'companies/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                
                $scope.companiesNumber = response.data['count']; 
                
              }, function errorCallback(response) {});

              $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
                function successCallback(response) {
                $scope.levelsNumber = response.data['count'];
              });

              $http({url: $rootScope.baseURLBackend+'all-stages/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.stagesNumber = response.data['count']; 

              });

              $http({url: $rootScope.baseURLBackend+'number-graduated-companies/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.numberGraduated = response.data['result']; 

              });

              $http({url: $rootScope.baseURLBackend+'total-number-employees/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.numberEmployees = response.data['result']; 

              });

              $http({url: $rootScope.baseURLBackend+'total-salary/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.totalSalary = response.data['result']; 

              });

              $http({url: $rootScope.baseURLBackend+'incubator-earnings/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.incubatorEarnings = response.data['result']; 

              });


            } else if ($rootScope.userProfile.permissionGroup.name == 'Mentores'){
              $rootScope.showAdmin = false;
              $rootScope.showMentor = true;
            } else {
              $rootScope.showAdmin = false;
            }
            
          }, function errorCallback(response) {});

        }, function errorCallback(response) {});
        

        
        
        

      }, 
      function errorCallback(response) {
        //$window.location.href = $rootScope.baseURL;
       
      }
 
    );
  
  
});


/*Main Controller (Dashboard + Login)*/
mainApp.controller('MainController', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $route, $interval) {
  
  $interval(function(){
        $rootScope.main_message = "";
  },10000);
  
  $scope.collapse = function() {
    if($scope.collapseClass == "sidebar-collapse"){
      $scope.collapseClass = "";
    } else {
      $scope.collapseClass = "sidebar-collapse";
    }
  };
  $scope.no_collapse = function() {
    var screenWidth = $window.innerWidth;
    if (screenWidth < 767){
      
      $( "#button-navigation" ).trigger( "click" );
      
    } 
  };
  $scope.messagesBox = function() {
    if($scope.messagesClass == "open"){
      $scope.messagesClass = "";
    } else {
      $scope.messagesClass = "open";
    }
  };

  $scope.notificationsBox = function() {
    if($scope.notificationsClass == "open"){
      $scope.notificationsClass = "";
    } else {
      $scope.notificationsClass = "open";
    }
  };



  $scope.userBox = function() {
    if($scope.userClass == "open"){
      $scope.userClass = "";
    } else {
      $scope.userClass = "open";
    }
  };
  $scope.filemanager = function() {
    $window.location.href = $rootScope.baseURL+'filemanager/?lang=es';
  };
  $scope.home = function() {
    $window.location.href = $rootScope.baseURL;
  };
  $scope.logout = function () {
    $http.post($rootScope.baseURLBackend+'rest-auth/logout/', $scope.user).then(
      function successCallback(response) {
      $scope.message = 'Exito';   
      var token = $cookies.remove('tokenpts');
      $window.location.href = $rootScope.baseURL;

    }, function errorCallback(response) {
     
    });
  };

  //User authentication validation
  var token = $cookies.get('tokenpts');
  $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
        $rootScope.loginShow = false;
        $rootScope.page_title = "Dashboard"
        //$scope.message = angular.fromJson(response); 
        $scope.dashboardShow = true;
        $scope.user_complete_name = response.data['first_name']+" "+response.data['last_name'];
        //Calculating User ID:
        $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $rootScope.userID = response.data['pk']; 

  
            //Calculating User Profile
            $http({url: $rootScope.baseURLBackend+'get-user-profile/'+$rootScope.userID+'/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {
              
              
              $rootScope.userProfile = response.data['results'][0]; 

              //User View Management
              //Change to customize
              if ($rootScope.userProfile.permissionGroup.name == 'Administradores'){
                $rootScope.showAdmin = true;
              } else if ($rootScope.userProfile.permissionGroup.name == 'Mentores'){
                $rootScope.showAdmin = false;
                $rootScope.showMentor = true;
                
              } else {
                $rootScope.showAdmin = false;
              } 
              
            }, function errorCallback(response) {});       
            
          
            
          }, function errorCallback(response) {});
        
      }, 
      //Login handler:
      function errorCallback(response) {
        $rootScope.loading = false;

        $scope.dashboardShow = false;
        $rootScope.loginShow = true;
        $rootScope.page_title = "Dashboard"

        $scope.submit = function () {
          //$rootScope.loading = true;
          $http.post($rootScope.baseURLBackend+'rest-auth/login/', $scope.user).then(
            function successCallback(response) {
              
              key = angular.fromJson(response); 
              $cookies.put('tokenpts', key['data']['key']);
              

              $window.location.reload();
              

              
            }, function errorCallback(response) {
              $rootScope.loading = false;
              $scope.message = 'Error: Usuario o clave inválidos';

          });

        };
        //Sign Up Handler:
        $scope.signUp = function () {
          $rootScope.loginShow = false;
          $scope.signUpShow = true;
          $rootScope.page_title = "Dashboard"
          $scope.signingUp = function () {

            $http.post($rootScope.baseURLBackend+'rest-auth/registration/', $scope.usersign).then(function successCallback(response) {
              
             key = angular.fromJson(response); 
             $cookies.put('tokenpts', key['data']['key']);
             
             $window.location.reload();

             

             
            }, function errorCallback(response) {
             errors = angular.fromJson(response); 
             $scope.message = errors['data'];
           });    
          };

          
        };
      }
 
    );
  
  
});







angular.module('FileManagerApp').config(['fileManagerConfigProvider', function (config) {
  var defaults = config.$get();
  config.set({
    appName: 'angular-filemanager',
    pickCallback: function(item) {
      var msg = 'Picked %s "%s" for external use'
        .replace('%s', item.type)
        .replace('%s', item.fullPath());
      window.alert(msg);
    },

    allowedActions: angular.extend(defaults.allowedActions, {
      pickFiles: true,
      pickFolders: false,
    }),
  });
}]);

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});