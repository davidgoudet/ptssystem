/*Room Principal*/
mainApp.controller('Rooms', function ($scope, $http, $window, $cookies, $route, $cookieStore, $routeParams, $rootScope, $location) {
  
  $rootScope.page_title = "Salas del Foro"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'rooms/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
    $scope.rooms = response.data['results']; 
    
  $scope.delete = function(id) {
    var deleteElement = $window.confirm('¿Seguro que deseas eliminar?');

    if (deleteElement) {
      $http({url: $rootScope.baseURLBackend+'room-delete/'+id+'/', method: 'DELETE', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          
          $route.reload();

        }, 
        function errorCallback(response) {});
    }
  }

    
  }, function errorCallback(response) {});
  
});

/*Room Creation*/
mainApp.controller('RoomsCreate', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope','$http', '$routeParams', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams) {
  
  $rootScope.page_title = "Crear Sala del Foro"
  var token = $cookies.get('tokenpts');


$scope.send = function() {
  

  
    $http.post($rootScope.baseURLBackend+'room-create/', $scope.new_room, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/salas/" );
      $rootScope.main_message = "Sala Creada"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  

}
}]);

