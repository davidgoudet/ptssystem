
/*Templates Principal*/
mainApp.controller('Templates', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $route) {
  
  $rootScope.page_title = "Plantillas"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'templates/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
      $scope.message = response.data;
      $scope.templates = response.data['results']; 

    $http({url: $rootScope.baseURLBackend+'all-stages/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
        $scope.message = response.data;
        $scope.stages = response.data['results']; 
      });
    $scope.delete = function (id) {
      var deleteElement = $window.confirm('¿Seguro que deseas eliminar?');

      if (deleteElement) {
        $http({url: $rootScope.baseURLBackend+'template-detail/'+id+'/', method: 'DELETE', headers: {
        'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            
            $route.reload();
            $rootScope.main_message = "Plantilla Eliminada"

          }, 
          function errorCallback(response) {});
          }
      }
    
  }, function errorCallback(response) {});
  
});

/*Templates Creation*/
mainApp.controller('TemplatesCreate', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Crear Plantilla"
  $scope.send = function () {
    var token = $cookies.get('tokenpts');
    $http.post($rootScope.baseURLBackend+'template/', $scope.new_template, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/plantillas" );
      $rootScope.main_message = "Plantilla Creada"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }
});

/*Templates Edition*/
mainApp.controller('TemplatesEdit', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams) {
  
  $rootScope.page_title = "Editar Plantilla"
  var token = $cookies.get('tokenpts');
 
  $http({url: $rootScope.baseURLBackend+'template-detail/'+$routeParams.templateId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_template = response.data;


      $scope.send = function () {
        
        $http.put($rootScope.baseURLBackend+'template-detail/'+$routeParams.templateId+'/', $scope.new_template, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/plantillas" );
          $rootScope.main_message = "Plantilla Editada"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);

        });
      }

      $scope.delete = function (id) {
        $http({url: $rootScope.baseURLBackend+'template-detail/'+$routeParams.templateId+'/', method: 'DELETE', headers: {
        'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            $location.path( "/plantillas" );
            $rootScope.main_message = "Plantilla Eliminada"
          }, 
          function errorCallback(response) {});
            
        }

    }
  )

  
  
  
});
