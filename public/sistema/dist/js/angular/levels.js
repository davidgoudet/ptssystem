
/*Levels Principal*/
mainApp.controller('Levels', function ($scope, $route, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Niveles"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    $scope.message = response.data;
    $scope.levels = response.data['results']; 

    $scope.delete = function (id) {
      var deleteElement = $window.confirm('¿Seguro que deseas eliminar?');

        if (deleteElement) {
      $http({url: $rootScope.baseURLBackend+'level-detail/'+id, method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $scope.message = response.data;
          $route.reload();

        }, 
        function errorCallback(response) {});
          }
    }
    
  }, function errorCallback(response) {});
  
});

  
/*Level Creation*/
mainApp.controller('LevelsCreate', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', function ($scope, Upload, $timeout, $cookies, $location, $rootScope) {
    var token = $cookies.get('tokenpts');
    
    $scope.uploadPic = function(file) {
    file.upload = Upload.upload({
      url: $rootScope.baseURLBackend+'level/',
      data: {level: $scope.new_level, file: file},
      headers: {'Authorization': 'Token '+token}
    });

    file.upload.then(function (response) {
      $timeout(function () {
        $location.path( "/niveles" );
      });
    }, function (response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
    }
}]);



/*Levels Edition*/
mainApp.controller('LevelEdit', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', '$http', '$routeParams', '$route', '$filter','$templateCache', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams, $route, $filter, $templateCache) {
  
  $rootScope.page_title = "Editar Nivel"
  var token = $cookies.get('tokenpts');


  $http({url: $rootScope.baseURLBackend+'level-detail/'+$routeParams.levelId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      
      $scope.new_level = response.data;


    });


  

$scope.send = function(file) {
  file = file || 0;

  if (file!=0){
    file.upload = Upload.upload({
      url: $rootScope.baseURLBackend+'level-detail/'+$routeParams.levelId+'/',
      data: {level: $scope.new_level, file: file},
      headers: {'Authorization': 'Token '+token},
      method: 'PUT'
    });

    file.upload.then(function (response) {
      $timeout(function () {
        $templateCache.removeAll();
        $location.path( "/niveles" );
        $route.reload();
        $rootScope.main_message = "Nivel Editado"
      });
    }, function (response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  } else {
    $http({url: $rootScope.baseURLBackend+'level-detail/'+$routeParams.levelId+'/', method: 'PUT', headers: {
    'Authorization': 'Token '+token}, data: $scope.new_level}).then(
    function successCallback(response) {
      $location.path( "/niveles" );
      $rootScope.main_message = "Nivel Editado"
    });
  }
}

}]);