/*Stage Tasks*/
mainApp.controller('Tasks', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $route, $location, $routeParams) {
  
  $rootScope.page_title = "Tareas del Ciclo";
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'tasks/'+$routeParams.stageId, method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    $scope.thisStage = $routeParams.stageId;
    $scope.message = response.data;
    $scope.tasks = response.data['results']; 

    $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'task-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $route.reload();
          $rootScope.main_message = "Tarea Eliminada"
        }, 
        function errorCallback(response) {});
          
      }
    
  }, function errorCallback(response) {});
  
});

/*Tasks Creation*/
mainApp.controller('TasksCreate', function ($scope, $http, $window, $cookies, $filter, $cookieStore, $rootScope, $location, $routeParams) {
  var token = $cookies.get('tokenpts');
  $rootScope.page_title = "Crear Tarea"


  $http({url: $rootScope.baseURLBackend+'all-stages/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
      $scope.stages = response.data['results']; 
      var namevar = $filter('filter')($scope.stages, {id: $routeParams.stageId})[0];
      $scope.stageId = {id: $routeParams.stageId, name: namevar}

  });

  $scope.send = function () {
    $scope.new_task['stage'] = $scope.stageId.id
    $http.post($rootScope.baseURLBackend+'task/', $scope.new_task, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/tareas/"+$scope.stageId.id );
      $rootScope.main_message = "Tarea Creada"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }
});

/*Tasks Edition*/
mainApp.controller('TaskEdit', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams, $filter) {
  
  $rootScope.page_title = "Editar Tarea"
  var token = $cookies.get('tokenpts');

  
 
  $http({url: $rootScope.baseURLBackend+'task-detail/'+$routeParams.taskId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_task = response.data;
      var stageIdOriginal = response.data.stage
      

      $http({url: $rootScope.baseURLBackend+'all-stages/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $scope.stages = response.data['results']; 
          var namevar = $filter('filter')($scope.stages, {id: stageIdOriginal})[0];
          $scope.stageId = {id: stageIdOriginal, name: namevar}

      });

    

      $scope.send = function () {
        $scope.new_task['template'] = $scope.stageId.id
        $scope.new_task['id'] = $routeParams.taskId

        $http.put($rootScope.baseURLBackend+'task-detail/'+$routeParams.taskId+'/', $scope.new_task, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/tareas/"+$scope.stageId.id ); 
          $rootScope.main_message = "Tarea Editada"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);;

        });
      }

    

      $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'task-detail/'+id, method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $location.path( "/tareas/"+stageIdOriginal );
          $rootScope.main_message = "Tarea Eliminada"
        }, 
        function errorCallback(response) {});
          
      }

    }
  )

  
  
  
});

/*Tasks Review*/
mainApp.controller('TaskReview', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams, $filter) {
  
  $rootScope.page_title = "Revisar Tarea"
  var token = $cookies.get('tokenpts');

  
 
  $http({url: $rootScope.baseURLBackend+'task-story-detail/'+$routeParams.pendingObjectId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.pending_object = response.data;
            

      

    

      $scope.send = function () {
        

        $http.put($rootScope.baseURLBackend+'task-story-review/'+$routeParams.pendingObjectId+'/', $scope.new_task, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/pendientes" ); 
          $rootScope.main_message = "Entrega Revisada"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);;

        });
      }

    

      $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'task-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $location.path( "/tareas/"+stageIdOriginal );
          $rootScope.main_message = "Tarea Eliminada"
        }, 
        function errorCallback(response) {});
          
      }

    }
  )

  
  
  
});
