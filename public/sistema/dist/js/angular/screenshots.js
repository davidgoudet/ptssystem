/*Screenshots Principal*/
mainApp.controller('ScreenshotsCompany', function ($scope, $http, $window, $cookies, $route, $cookieStore, $routeParams, $rootScope, $location) {
  
  $rootScope.page_title = "Estados de la Empresa"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'screenshoots-company/'+$routeParams.companyId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
    $scope.screenshots = response.data['results']; 
    $scope.companyId = $routeParams.companyId;

    
  }, function errorCallback(response) {});
  
});

/*Screenshots Creation*/
mainApp.controller('ScreenshotsCompanyCreate', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope','$http', '$routeParams', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams) {
  
  $rootScope.page_title = "Crear Estado de Empresa"
  var token = $cookies.get('tokenpts');


$scope.send = function(file) {
  file = file || 0;

  if (file!=0){

  } else {
    $http.post($rootScope.baseURLBackend+'screenshoot-company/'+$routeParams.companyId+'/', $scope.new_screenshot, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/estados/"+$routeParams.companyId );
      $rootScope.main_message = "Estado Creado"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }

}
}]);

/*Screenshot Detail*/
mainApp.controller('ScreenshotCompanyDetail', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope','$http', '$routeParams', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams) {
  
  $rootScope.page_title = "Estado de la Empresa"
  var token = $cookies.get('tokenpts');

   $http({url: $rootScope.baseURLBackend+'screenshoot-company-detail/'+$routeParams.screenshotId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
    $scope.screenshot = response.data; 
    

    
  }, function errorCallback(response) {});

}]);

/*Screenshots Incubator Principal*/
mainApp.controller('ScreenshotsIncubator', function ($scope, $http, $window, $cookies, $route, $cookieStore, $routeParams, $rootScope, $location) {
  
  $rootScope.page_title = "Estados de la Incubadora"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'screenshoots-incubator/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
   $scope.screenshots = response.data['results']; 
   

    
  }, function errorCallback(response) {});
  
});

/*Screenshots Incubator Creation*/
mainApp.controller('ScreenshotsIncubatorCreate', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope','$http', '$routeParams', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams) {
  
  $rootScope.page_title = "Crear Estado de Incubadora"
  var token = $cookies.get('tokenpts');


$scope.send = function(file) {
  file = file || 0;

  if (file!=0){

  } else {
    $http.post($rootScope.baseURLBackend+'screenshoot-incubator/', $scope.new_screenshot, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/estados_incubadora/");
      $rootScope.main_message = "Estado Creado"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }

}
}]);
  

/*Screenshot Detail*/
mainApp.controller('ScreenshotIncubatorDetail', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope','$http', '$routeParams', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams) {
  
  $rootScope.page_title = "Estado de la Incubadora"
  var token = $cookies.get('tokenpts');

   $http({url: $rootScope.baseURLBackend+'screenshoot-incubator-detail/'+$routeParams.screenshotId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
    $scope.screenshot = response.data; 
    

    
  }, function errorCallback(response) {});

}]);


/*Screenshots Summary*/
mainApp.controller('ScreenshotSummary', function ($scope, $http, $window, $cookies, $route, $cookieStore, $routeParams, $rootScope, $location) {
  
  $rootScope.page_title = "Resumen de Estados de la Incubadora"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'screenshoots-incubator/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
   $scope.screenshots = response.data['results']; 
   

    
  }, function errorCallback(response) {});
  
});

/*Company Screenshots Summary*/
mainApp.controller('CompanyScreenshotSummary', function ($scope, $http, $window, $cookies, $route, $cookieStore, $routeParams, $rootScope, $location) {
  
  $rootScope.page_title = "Resumen de Estados de la Empresa"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'screenshoots-company/'+$routeParams.companyId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
   $scope.screenshots = response.data['results']; 
   

    
  }, function errorCallback(response) {});
  
});