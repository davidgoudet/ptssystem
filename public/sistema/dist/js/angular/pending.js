/*Pending Tasks Principal*/
mainApp.controller('Pending', function ($scope, $route, $http, $window, $cookies, $cookieStore, $rootScope, $location) {

  $rootScope.page_title = "Tareas Pendientes Por Revisar";
  var token = $cookies.get('tokenpts');

  //Calculating User ID:
  $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $rootScope.userID = response.data['pk']; 
    //Calculating User Profile
    $http({url: $rootScope.baseURLBackend+'get-user-profile/'+$rootScope.userID+'/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
  
  $http({url: $rootScope.baseURLBackend+'pending/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      
      $scope.pending = response.data['results']; 

      $http({url: $rootScope.baseURLBackend+'pending-challenges/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {        
        $scope.pendingChallenges = response.data['results'];
      });

      if ($rootScope.userProfile.permissionGroup.name == 'Mentores'){
        $http({url: $rootScope.baseURLBackend+'companies-mentor/', method: 'GET', headers: {
          'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            $scope.message = response.data;
            $scope.companies = response.data['results']; 

          }, function errorCallback(response) {});

        } else {


          $http({url: $rootScope.baseURLBackend+'companies/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {

              $scope.companies = response.data['results']; 

            }, function errorCallback(response) {}
            );



          }

        }, function errorCallback(response) {});

  });
});   
}

);
