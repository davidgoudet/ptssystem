/*Taxonomy Cycles*/
mainApp.controller('Cycles', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $route, $location, $routeParams) {
  
  $rootScope.page_title = "Ciclos de la Taxonomía";
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'cycles/'+$routeParams.taxonomyId, method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    $scope.thisTaxonomy = $routeParams.taxonomyId;
    $scope.message = response.data;
    $scope.cycles = response.data['results']; 

    $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'cycle-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $route.reload();
          $rootScope.main_message = "Ciclo Eliminado"
        }, 
        function errorCallback(response) {});
          
      }
    
  }, function errorCallback(response) {});
  
});

/*Cycles Creation*/
mainApp.controller('CyclesCreate', function ($scope, $http, $window, $cookies, $filter, $cookieStore, $rootScope, $location, $routeParams) {
  var token = $cookies.get('tokenpts');
  $rootScope.page_title = "Crear Ciclo"




  $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
        $scope.levels = response.data['results']; 
        $scope.levelId = $scope.levels[0];
    });

  $http({url: $rootScope.baseURLBackend+'taxonomies/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
      $scope.taxonomies = response.data['results']; 
      var namevar = $filter('filter')($scope.taxonomies, {id: $routeParams.taxonomyId})[0];
      $scope.taxonomyId = {id: $routeParams.taxonomyId, name: namevar}

  });

  $scope.send = function () {
    $scope.new_cycle['taxonomy'] = $scope.taxonomyId.id
    $scope.new_cycle['level'] = $scope.levelId.id

    $http.post($rootScope.baseURLBackend+'cycle/', $scope.new_cycle, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/ciclos/"+$scope.taxonomyId.id ); 
      $rootScope.main_message = "Ciclo Creado"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }
});

/*Cycles Edition*/
mainApp.controller('CycleEdit', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams, $filter) {
  
  $rootScope.page_title = "Editar Ciclo"
  var token = $cookies.get('tokenpts');

  
 
  $http({url: $rootScope.baseURLBackend+'cycle-detail/'+$routeParams.cycleId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_cycle = response.data;
      var taxonomyIdOriginal = response.data.taxonomy
      var levelIdOriginal = response.data.level
      

      $http({url: $rootScope.baseURLBackend+'taxonomies/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
          $scope.taxonomies = response.data['results']; 
          var namevar = $filter('filter')($scope.taxonomies, {id: taxonomyIdOriginal})[0];
          $scope.taxonomyId = {id: taxonomyIdOriginal, name: namevar}

      });

      $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
            $scope.levels = response.data['results']; 
            var namevar = $filter('filter')($scope.levels, {id: levelIdOriginal})[0];
            $scope.levelId = {id: levelIdOriginal, name: namevar}
           
      });

      $scope.send = function () {
        $scope.new_cycle['taxonomy'] = $scope.taxonomyId.id
        $scope.new_cycle['level'] = $scope.levelId.id
        $scope.new_cycle['id'] = $routeParams.cycleId

        $http.put($rootScope.baseURLBackend+'cycle-detail/'+$routeParams.cycleId+'/', $scope.new_cycle, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/ciclos/"+$scope.taxonomyId.id ); 
          $rootScope.main_message = "Ciclo Editado"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);

        });
      }

    

      $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'cycle-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $location.path( "/ciclos/"+taxonomyIdOriginal );
          $rootScope.main_message = "Ciclo Eliminado"
        }, 
        function errorCallback(response) {});
          
      }

    }
  )

  
  
  
});