/*List all users*/
mainApp.controller('AllUsers', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $route, $filter) {
  
  $rootScope.page_title = "Usuarios"
  var token = $cookies.get('tokenpts');

  $http({url: $rootScope.baseURLBackend+'permission-groups/', method: 'GET', headers: {
  	'Authorization': 'Token '+token} }).then(
	  function successCallback(response) {
	  	$scope.permissionGroups = response.data['results']; 
	  	$scope.permissionGroupId = {};
	  	$scope.companyId = {};
	  	$scope.companiesId = {};
	  	$scope.selectCompany = {};
	  	$scope.selectCompanyMentor = {};
	  	$scope.divCompanies = [];
	  	$scope.update = function(id){
	  		$http({url: $rootScope.baseURLBackend+'companies/', method: 'GET', headers: {
			  'Authorization': 'Token '+token} }).then(
			  function successCallback(response) {	
			    $scope.companies = response.data['results']; 
			    

			  }, function errorCallback(response) {});
			//Change to customize
	  		if($scope.permissionGroupId[id]['name'] == "JD Empresa"){
	  			$scope.selectCompany[id] = true;
	  		} else {
	  			$scope.selectCompany[id] = false;
	  		}

	  		if($scope.permissionGroupId[id]['name'] == "Mentores"){
	  			$scope.selectCompanyMentor[id] = true;
	  		} else {
	  			$scope.selectCompanyMentor[id] = false;
	  		}
	  		
	  		
	  	}
	  	$scope.change = function(id) {

	  		$scope.new_user_profile = {};
	  		$scope.new_user_profile['companiesId']= {};
	  		
	  		$scope.new_user_profile['permissionGroupId'] = $scope.permissionGroupId[id]['id'];
	  		
	  		$scope.new_user_profile['userProfileId'] = id;

	  		//Change to customize
	  		if((($scope.permissionGroupId[id]['name'] == "JD Empresa") && (typeof $scope.companyId[id] != 'undefined')) || ($scope.permissionGroupId[id]['name'] != "JD Empresa" && $scope.permissionGroupId[id]['name'] != "Mentores")) {
	  			
	  			if(typeof $scope.companyId[id] != 'undefined'){
	  				$scope.new_user_profile['companyId'] = $scope.companyId[id]['id'];
	  			}
		  		$http({url: $rootScope.baseURLBackend+'user-profile-detail/'+id+'/', method: 'PUT', headers: {
			    'Authorization': 'Token '+token}, data: $scope.new_user_profile}).then(
			    function successCallback(response) {
			      $location.path( "/usuarios" );
			      $route.reload();
			      $rootScope.main_message = "Usuario Editado"
			    });
		    }
		    if(($scope.permissionGroupId[id]['name'] == "Mentores")){
		    	if(typeof $scope.companiesId[id] != 'undefined'){
	  				
	  				i=1;
	  				var compIds=[];
	  				for (var company in $scope.companiesId[id]) {
	  					compIds.push({"company_id":$scope.companiesId[id][i]['id']});
	  					i=i+1;
	  				}
	  				$scope.new_user_profile['companiesId'] = compIds;
	  			}
	  			$http({url: $rootScope.baseURLBackend+'user-profile-detail/'+id+'/', method: 'PUT', headers: {
			    'Authorization': 'Token '+token}, data: $scope.new_user_profile}).then(
			    function successCallback(response) {
			      $location.path( "/usuarios" );
			      $route.reload();
			      $rootScope.main_message = "Usuario Editado"
			    });
		    	//alert($scope.companiesId[id][1]['id']+"s"+$scope.companiesId[id][2]['id']);
		    }
	  	}

	  	$scope.addCompany = function(divCompanies) {

	  		divCompanies.push({select_id: Object.keys(divCompanies).length+1});
	  		
	  	}
	  	
	  }, function errorCallback(response) {}
  );


  $http({url: $rootScope.baseURLBackend+'user-profiles/', method: 'GET', headers: {
	  'Authorization': 'Token '+token} }).then(
	  function successCallback(response) {

	    $scope.user_profiles = response.data['results']; 
	  }, function errorCallback(response) {}
  );
  
});

