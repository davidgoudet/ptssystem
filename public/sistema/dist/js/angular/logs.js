
/*Logs Principal*/
mainApp.controller('Logs', function ($scope, $route, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Historias"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'logs/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    
    $scope.logs = response.data['results']; 

   
    
  }, function errorCallback(response) {});
  
});
