
/*Taxonomies Principal*/
mainApp.controller('Taxonomies', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $route) {
  
  $rootScope.page_title = "Taxonomías"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'taxonomies/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
      $scope.message = response.data;
      $scope.taxonomies = response.data['results']; 

    $http({url: $rootScope.baseURLBackend+'all-cycles/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
        $scope.message = response.data;
        $scope.cycles = response.data['results']; 
      });
    $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'taxonomy-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          
          $route.reload();
          $rootScope.main_message = "Taxonomía Eliminada"

        }, 
        function errorCallback(response) {});
          
      }
    
  }, function errorCallback(response) {});
  
});

/*Taxonomies Creation*/
mainApp.controller('TaxonomiesCreate', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Crear Taxonomía"
  $scope.send = function () {
    var token = $cookies.get('tokenpts');
    $http.post($rootScope.baseURLBackend+'taxonomy/', $scope.new_taxonomy, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/taxonomias" );
      $rootScope.main_message = "Taxonomía Creada"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }
});

/*Taxonomies Edition*/
mainApp.controller('TaxonomiesEdit', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams) {
  
  $rootScope.page_title = "Editar Taxonomía"
  var token = $cookies.get('tokenpts');
 
  $http({url: $rootScope.baseURLBackend+'taxonomy-detail/'+$routeParams.taxonomyId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_taxonomy = response.data;


      $scope.send = function () {
        
        $http.put($rootScope.baseURLBackend+'taxonomy-detail/'+$routeParams.taxonomyId+'/', $scope.new_taxonomy, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/taxonomias" );
          $rootScope.main_message = "Taxonomía Editada"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);

        });
      }

      $scope.delete = function (id) {
        $http({url: $rootScope.baseURLBackend+'taxonomy-detail/'+$routeParams.taxonomyId+'/', method: 'DELETE', headers: {
        'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            $location.path( "/taxonomias" );
            $rootScope.main_message = "Taxonomía Eliminada"
          }, 
          function errorCallback(response) {});
            
        }

    }
  )

  
  
  
});
