/*Home of Company User*/
mainApp.controller('CompanyUserHome', function ($scope, $route, $http, $window, $cookies, $cookieStore, $rootScope, $location) {

  $rootScope.page_title = "Inicio"
  var token = $cookies.get('tokenpts');

  $http({url: $rootScope.baseURLBackend+'companies/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.message = response.data;
      $scope.companies = response.data['results']; 
      $scope.companyId = $scope.companies[0];
      $rootScope.currentCompany = $scope.companies[0];


      if($rootScope.currentCompany.level != null){
      $http({url: $rootScope.baseURLBackend+'level-detail/'+$rootScope.currentCompany.level.id+'/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {        
          $scope.level = response.data; 
        }
        );
      }
      if($rootScope.currentCompany.current_stage_template != null){
        $http({url: $rootScope.baseURLBackend+'stage-detail/'+$rootScope.currentCompany.current_stage_template+'/', method: 'GET', headers: {
          'Authorization': 'Token '+token} }).then(
          function successCallback(response) {        
            $scope.stage = response.data; 
          }
          );
      } 
          $http({url: $rootScope.baseURLBackend+'company-points/'+$rootScope.currentCompany.id+'/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {        
             $scope.points = response.data['results']; 
           }
           );


        if($rootScope.currentCompany.current_stage_template != null){
          $http({url: $rootScope.baseURLBackend+'task-stories-stage-company/'+$rootScope.currentCompany.current_stage_template+'/'+$rootScope.currentCompany.id+'/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {        
             $scope.task_stories = response.data['results']; 
           }
           );
        }
            $http({url: $rootScope.baseURLBackend+'badges-users-or-company/'+$rootScope.currentCompany.id+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {        
               $scope.all_badges = response.data['results']; 
             }
             );

              $http({url: $rootScope.baseURLBackend+'badges-users-or-company/'+$rootScope.currentCompany.id+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {        
                
               $scope.company_badges = response.data['company_badges']; 

               $scope.descriptionBadgeF = function(badge) {
                $scope.descriptionBadge = badge.name+" - "+badge.description;
               }
               
             }
             );


           


            $scope.changeCompany = function (id) {
              $http({url: $rootScope.baseURLBackend+'company-detail/'+id+'/', method: 'GET', headers: {
                'Authorization': 'Token '+token} }).then(
                function successCallback(response) {
                  $rootScope.currentCompany = response.data;

                  $http({url: $rootScope.baseURLBackend+'level-detail/'+$rootScope.currentCompany.level.id+'/', method: 'GET', headers: {
                    'Authorization': 'Token '+token} }).then(
                    function successCallback(response) {        
                      $scope.level = response.data; 
                    }
                    );

                    $http({url: $rootScope.baseURLBackend+'stage-detail/'+$rootScope.currentCompany.current_stage_template+'/', method: 'GET', headers: {
                      'Authorization': 'Token '+token} }).then(
                      function successCallback(response) {        
                        $scope.stage = response.data; 
                      }
                      );

                      $http({url: $rootScope.baseURLBackend+'task-stories-stage-company/'+$rootScope.currentCompany.current_stage_template+'/'+$rootScope.currentCompany.id+'/', method: 'GET', headers: {
                        'Authorization': 'Token '+token} }).then(
                        function successCallback(response) {        
                         $scope.task_stories = response.data['results']; 
                       }
                       );

                      });
}
});



});

/*Tasks Completion*/

mainApp.controller('CompleteTask', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', '$http', '$routeParams', '$route', '$filter', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams, $route, $filter) { 
  $rootScope.page_title = "Completar Tarea"
  var token = $cookies.get('tokenpts');

  

  $http({url: $rootScope.baseURLBackend+'task-story-detail/'+$routeParams.taskId, method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_task_story = response.data;
      
      



      $scope.send = function (file) {
        if ($scope.new_task_story.story_have_not_template != true){
          $scope.new_task_story.task_template = $scope.new_task_story.task.id
          $scope.new_task_story.current_company = $rootScope.currentCompany.id
        } 
        
        

        file = file || 0;

        if (file!=0){
          file.upload = Upload.upload({
            url: $rootScope.baseURLBackend+'task-story-detail/'+$scope.new_task_story.id+'/',
            data: {task_story: $scope.new_task_story, file: file},
            headers: {'Authorization': 'Token '+token},
            method: 'PUT'
          });

          file.upload.then(function (response) {
            $timeout(function () {
              if ($rootScope.userProfile.permissionGroup.name == 'JD Empresa'){
                $location.path( "/tablero_empresa" );
              } else {
                $location.path( "/tablero" );
              }
              $route.reload();
              $rootScope.main_message = "Tarea enviada"
            });
          }, function (response) {
            if (response.status > 0)
              $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
        } else {

          $http.put($rootScope.baseURLBackend+'task-story-detail/'+$scope.new_task_story.id+'/', $scope.new_task_story, {headers: {
            'Authorization': 'Token '+token}}).then(
            function successCallback(response) {
              $location.path( "/tablero" );
              $rootScope.main_message = "Tarea enviada"
            }, function errorCallback(response) {
              errors = angular.fromJson(response); 
              console.log(errors['data']);
            });



          }

        }



        $scope.delete = function (id) {
          $http({url: $rootScope.baseURLBackend+'task-detail/'+id, method: 'DELETE', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {
              $location.path( "/tareas/"+stageIdOriginal );
              $rootScope.main_message = "Tarea Eliminada"
            }, 
            function errorCallback(response) {});

          }

        }
        )




}]);

/*Challenge Completion*/

mainApp.controller('CompleteChallenge', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', '$http', '$routeParams', '$route', '$filter', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams, $route, $filter) { 
  $rootScope.page_title = "Completar Desafío"
  var token = $cookies.get('tokenpts');

  

  $http({url: $rootScope.baseURLBackend+'challenge-detail/'+$routeParams.challengeId, method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_challenge = response.data;
      
      



      $scope.send = function (file) {
        

        file = file || 0;

        if (file!=0){
          file.upload = Upload.upload({
            url: $rootScope.baseURLBackend+'challenge-detail/'+$scope.new_challenge.id+'/',
            data: {task_story: $scope.new_challenge, file: file},
            headers: {'Authorization': 'Token '+token},
            method: 'PUT'
          });

          file.upload.then(function (response) {
            $timeout(function () {
              $location.path( "/tablero" );              
              $route.reload();
              $rootScope.main_message = "Desafío enviado"
            });
          }, function (response) {
            if (response.status > 0)
              $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
        } else {

          $http.put($rootScope.baseURLBackend+'challenge-detail/'+$scope.new_challenge.id+'/', $scope.new_challenge, {headers: {
            'Authorization': 'Token '+token}}).then(
            function successCallback(response) {
              $location.path( "/tablero" );
              $rootScope.main_message = "Desafío enviado"
            }, function errorCallback(response) {
              errors = angular.fromJson(response); 
              console.log(errors['data']);
            });



          }

        }



      

        }
        )




}]);

/*Dashboard for Company User (No admin logged)*/
mainApp.controller('CompanyDashboard', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  $rootScope.loading = false;
  $rootScope.page_title = "Inicio"
  var token = $cookies.get('tokenpts');
  //Calculating User ID:
  $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $rootScope.userID = response.data['pk'];
  //Calculating User Profile
  $http({url: $rootScope.baseURLBackend+'get-user-profile/'+$rootScope.userID+'/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {


      $rootScope.userProfile = response.data['results'][0]; 
      $scope.currentCompany = $rootScope.userProfile.company;

      $http({url: $rootScope.baseURLBackend+'level-read/'+$rootScope.userProfile.company.level.id+'/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {        
          $scope.level = response.data; 
        }
        );

        $http({url: $rootScope.baseURLBackend+'task-stories-stage-company/'+$rootScope.userProfile.company.current_stage_template+'/'+$rootScope.userProfile.company.id+'/', method: 'GET', headers: {
          'Authorization': 'Token '+token} }).then(
          function successCallback(response) {        
           var task_stories = response.data['results']; 

           
        $http({url: $rootScope.baseURLBackend+'task-stories-user/'+$rootScope.userID+'/'+$rootScope.userProfile.company.current_stage_template+'/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {    
          
          
          task_stories.push(response.data['results']);
          var unwraped = [].concat.apply([], task_stories);    
          $scope.task_stories = unwraped;

          
          var completedTasks = 0;
          $scope.task_stories.forEach(function(entry) {
            if (entry.accepted){
              completedTasks = completedTasks + 1;
            }
            
          });

          $scope.progress = ((completedTasks*100) / $scope.task_stories.length).toFixed(2);
          
         }
         );

         }
         );

          $http({url: $rootScope.baseURLBackend+'stage-read/'+$rootScope.userProfile.company.current_stage_template+'/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {        
              $scope.stage = response.data; 
            }
            );

            $http({url: $rootScope.baseURLBackend+'company-points/'+$rootScope.userProfile.company.id+'/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {        
             $scope.points = response.data['results']; 
           }
           );
            
           $http({url: $rootScope.baseURLBackend+'challenges/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {
              $scope.challenges = response.data['results'];
            });

            $http({url: $rootScope.baseURLBackend+'badges-users-or-company/'+$rootScope.userProfile.company.id+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {        
              
               $scope.company_badges = response.data['company_badges']; 
               $scope.user_badges = response.data['user_badges']; 

               $scope.descriptionBadgeF = function(badge) {
                $scope.descriptionBadge = badge.name+" - "+badge.description;
               }
             }
             );

          $scope.changeFilter = function() {
            if($scope.typeOfTask=="challenges"){
              $http({url: $rootScope.baseURLBackend+'challenges/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.challenges = response.data['results'];
              });
              $scope.task_stories = {}; 
            } else if ($scope.typeOfTask=="pending"){
              $http({url: $rootScope.baseURLBackend+'task-stories-user/'+$rootScope.userID+'/'+$rootScope.userProfile.company.current_stage_template+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                task_stories = response.data['results'];
                var task_stories_pending = [];
                $scope.task_stories = {};
                task_stories.forEach(function(entry) {
                  if (!entry.sent && !entry.accepted){
                    task_stories_pending.push(entry)
                    
                  }
                
                });
                $scope.challenges = {};
                $scope.task_stories = task_stories_pending;
                
                
               }
               );
            } else if ($scope.typeOfTask=="sent"){
              $http({url: $rootScope.baseURLBackend+'task-stories-user/'+$rootScope.userID+'/'+$rootScope.userProfile.company.current_stage_template+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                task_stories = response.data['results'];
                var task_stories_pending = [];
                $scope.task_stories = {};
                task_stories.forEach(function(entry) {
                  if (entry.sent){
                    task_stories_pending.push(entry)
                    
                  }
                
                });
                $scope.challenges = {};
                $scope.task_stories = task_stories_pending;
                
                
               }
               );
            }  else if ($scope.typeOfTask=="ready"){
              $http({url: $rootScope.baseURLBackend+'task-stories-user/'+$rootScope.userID+'/'+$rootScope.userProfile.company.current_stage_template+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                task_stories = response.data['results'];
                var task_stories_accepted = [];
                $scope.task_stories = {};
                task_stories.forEach(function(entry) {
                  if (entry.accepted){
                    task_stories_accepted.push(entry)
                    
                  }
                
                });
                $scope.challenges = {};
                $scope.task_stories = task_stories_accepted;
                
                
               }
               );
            } else if ($scope.typeOfTask=="pending-challenges"){
              $http({url: $rootScope.baseURLBackend+'challenges/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                challenges = response.data['results'];
                var challenges_pending = [];
                $scope.challenges = {};
                challenges.forEach(function(entry) {
                  if (!entry.sent && !entry.accepted){
                    challenges_pending.push(entry)
                    
                  }
                
                });
                $scope.task_stories = {};
                $scope.challenges = challenges_pending;
                
                
               }
               );
            } else if ($scope.typeOfTask=="sent-challenges"){
              $http({url: $rootScope.baseURLBackend+'challenges/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                challenges = response.data['results'];
                var challenges_pending = [];
                $scope.challenges = {};
                challenges.forEach(function(entry) {
                  if (entry.sent){
                    challenges_pending.push(entry)
                    
                  }
                
                });
                $scope.task_stories = {};
                $scope.challenges = challenges_pending;
                
                
               }
               );
            } else if ($scope.typeOfTask=="challenges-ready"){
              $http({url: $rootScope.baseURLBackend+'challenges/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                challenges = response.data['results'];
                var challenges_pending = [];
                $scope.challenges = {};
                challenges.forEach(function(entry) {
                  if (entry.accepted){
                    challenges_pending.push(entry)
                    
                  }
                
                });
                $scope.task_stories = {};
                $scope.challenges = challenges_pending;
                
                
               }
               );
            } else if ($scope.typeOfTask=="all-tasks"){
              $http({url: $rootScope.baseURLBackend+'task-stories-user/'+$rootScope.userID+'/'+$rootScope.userProfile.company.current_stage_template+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {    
                $scope.challenges = {};
                $scope.task_stories = response.data['results'];
                
                
               }
               );
            }



          }

          });
  });
});


/*Task Creation for Company User (No admin logged)*/
mainApp.controller('TasksCreateCompanyUser', function ($scope, $http, $window, $cookies, $filter, $routeParams, $cookieStore, $rootScope, $location) {
 var token = $cookies.get('tokenpts');
  $rootScope.page_title = "Crear Tarea"


  $http({url: $rootScope.baseURLBackend+'all-stages/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
      $scope.stages = response.data['results']; 
      var namevar = $filter('filter')($scope.stages, {id: $routeParams.stageId})[0];
      $scope.stageId = {id: $routeParams.stageId, name: namevar}

  });

  $http({url: $rootScope.baseURLBackend+'task-areas/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
      
      $scope.task_areas = response.data['results']; 
    }, function errorCallback(response) {});

  $http({url: $rootScope.baseURLBackend+'status/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
      
      $scope.all_status = response.data['results']; 
    }, function errorCallback(response) {});

  $http({url: $rootScope.baseURLBackend+'users/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
      
      $scope.users = response.data['results']; 
    }, function errorCallback(response) {});

  $scope.save = function() {      
    //We save the task as a TaskStory without template
    
    if($scope.task_story['start_date'] != undefined && $scope.task_story['ending_date'] != undefined && $scope.task_story['task_area_id'] != undefined && $scope.task_story['status_id'] != undefined && $scope.task_story['points'] != undefined && angular.element(document.querySelector('#in_charge_id_value')).val() != "" && $scope.task_story['stageId'] != undefined){
      $scope.task_story_post = {start_date: {}, ending_date: {}}; 
      $scope.task_story_post.start_date = $scope.task_story['start_date'];
      $scope.task_story_post.ending_date = $scope.task_story['ending_date'];
      $scope.task_story_post.points = $scope.task_story['points'];
      $scope.task_story_post.task_area_id = $scope.task_story['task_area_id']['id'];  
      $scope.task_story_post.status_id = $scope.task_story['status_id']['id'];
      $scope.task_story_post.description_not_template = $scope.task_story['description'];
      $scope.task_story_post.name_not_template = $scope.task_story['name'];
      $scope.task_story_post.stage_id = $scope.task_story['stageId']['id'];
      
      
      $scope.task_story_post.in_charge_username = angular.element(document.querySelector('#in_charge_id_value')).val();
          


     $http.post($rootScope.baseURLBackend+'task-story/', $scope.task_story_post,{headers: {
      'Authorization': 'Token '+token}}).then(
        function successCallback(response) {
          if ($rootScope.userProfile.permissionGroup.name == 'Mentores' || $rootScope.userProfile.permissionGroup.name == 'Administradores' || $rootScope.userProfile.permissionGroup.name == 'Gerentes'){
            $location.path( "/tablero" );
          } if ($rootScope.userProfile.permissionGroup.name == 'JD Empresa'){
            $location.path( "/tablero_empresa" );  
          }
          
          $rootScope.main_message = "Tarea Creada"
        }, function errorCallback(response) {
          errors = angular.fromJson(response); 
          console.log(errors['data']);

      });

    } else {
      $scope.errorMessage = "Debe llenar todos los campos";
    }
    
  }
});
