/*Template Stages*/
mainApp.controller('Stages', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $route, $location, $routeParams) {
  
  $rootScope.page_title = "Etapas de la Plantilla";
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'stages/'+$routeParams.templateId, method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    $scope.thisTemplate = $routeParams.templateId;
    $scope.message = response.data;
    $scope.stages = response.data['results']; 

    $scope.delete = function (id) {
      var deleteElement = $window.confirm('¿Seguro que deseas eliminar?');

        if (deleteElement) {
      $http({url: $rootScope.baseURLBackend+'stage-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $route.reload();
          $rootScope.main_message = "Etapa Eliminada"
        }, 
        function errorCallback(response) {});
          
      }
    }
    
  }, function errorCallback(response) {});
  
});

/*Stages Creation*/
mainApp.controller('StagesCreate', function ($scope, $http, $window, $cookies, $filter, $cookieStore, $rootScope, $location, $routeParams) {
  var token = $cookies.get('tokenpts');
  $rootScope.page_title = "Crear Etapa"




  $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
        $scope.levels = response.data['results']; 
        $scope.levelId = $scope.levels[0];
    });

  $http({url: $rootScope.baseURLBackend+'templates/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
      $scope.templates = response.data['results']; 
      var namevar = $filter('filter')($scope.templates, {id: $routeParams.templateId})[0];
      $scope.templateId = {id: $routeParams.templateId, name: namevar}

  });

  $scope.send = function () {
    $scope.new_stage['template'] = $scope.templateId.id
    $scope.new_stage['level'] = $scope.levelId.id

    $http.post($rootScope.baseURLBackend+'stage/', $scope.new_stage, {headers: {
    'Authorization': 'Token '+token}}).then(
    function successCallback(response) {
      $location.path( "/etapas/"+$scope.templateId.id ); 
      $rootScope.main_message = "Etapa Creada"
    }, function errorCallback(response) {
        errors = angular.fromJson(response); 
        console.log(errors['data']);

    });
  }
});

/*Stages Edition*/
mainApp.controller('StageEdit', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams, $filter) {
  
  $rootScope.page_title = "Editar Etapa"
  var token = $cookies.get('tokenpts');

  
 
  $http({url: $rootScope.baseURLBackend+'stage-detail/'+$routeParams.stageId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_stage = response.data;
      var templateIdOriginal = response.data.template
      var levelIdOriginal = response.data.level
      

      $http({url: $rootScope.baseURLBackend+'templates/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
          $scope.templates = response.data['results']; 
          var namevar = $filter('filter')($scope.templates, {id: templateIdOriginal})[0];
          $scope.templateId = {id: templateIdOriginal, name: namevar}

      });

      $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
            $scope.levels = response.data['results']; 
            var namevar = $filter('filter')($scope.levels, {id: levelIdOriginal})[0];
            $scope.levelId = {id: levelIdOriginal, name: namevar}
           
      });

      $scope.send = function () {
        $scope.new_stage['template'] = $scope.templateId.id
        $scope.new_stage['level'] = $scope.levelId.id
        $scope.new_stage['id'] = $routeParams.stageId

        $http.put($rootScope.baseURLBackend+'stage-detail/'+$routeParams.stageId+'/', $scope.new_stage, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/etapas/"+$scope.templateId.id ); 
          $rootScope.main_message = "Etapa Editada"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);

        });
      }

    

      $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'stage-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $location.path( "/etapas/"+templateIdOriginal );
          $rootScope.main_message = "Etapa Eliminada"
        }, 
        function errorCallback(response) {});
          
      }

    }
  )

  
  
  
});