
/*Badges Principal*/
mainApp.controller('Badges', function ($scope, $route, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Insignias"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'badges/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    $scope.message = response.data;
    $scope.badges = response.data['results']; 

    $scope.delete = function (id) {
      var deleteElement = $window.confirm('¿Seguro que deseas eliminar?');

      if (deleteElement) {
        
      
        $http({url: $rootScope.baseURLBackend+'badge-detail/'+id+'/', method: 'DELETE', headers: {
        'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            
            $route.reload();

          }, 
          function errorCallback(response) {});
      }
    }
    
  }, function errorCallback(response) {});
  
});

  
/*Badge Creation*/
mainApp.controller('BadgesCreate', ['$scope', 'Upload', '$timeout', '$cookies', '$location', function ($scope, Upload, $timeout, $cookies, $location) {
    var token = $cookies.get('tokenpts');
    
    $scope.uploadPic = function(file) {
    file.upload = Upload.upload({
      url: 'http://pts.hezionstudios.com/badge/',
      data: {badge: $scope.new_badge, file: file},
      headers: {'Authorization': 'Token '+token}
    });

    file.upload.then(function (response) {
      $timeout(function () {
        $location.path( "/insignias" );
      });
    }, function (response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
    }
}]);



/*Badges Edition*/
mainApp.controller('BadgeEdit', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', '$http', '$routeParams', '$route', '$filter', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams, $route, $filter) {
  
  $rootScope.page_title = "Editar Insignia";
  var token = $cookies.get('tokenpts');


  $http({url: $rootScope.baseURLBackend+'badge-detail/'+$routeParams.badgeId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      
      $scope.new_badge = response.data;


    });


  

$scope.send = function(file) {
  file = file || 0;

  if (file!=0){
    file.upload = Upload.upload({
      url: $rootScope.baseURLBackend+'badge-detail/'+$routeParams.badgeId+'/',
      data: {badge: $scope.new_badge, file: file},
      headers: {'Authorization': 'Token '+token},
      method: 'PUT'
    });

    file.upload.then(function (response) {
      $timeout(function () {
        $location.path( "/insignias" );
        $route.reload();
        $rootScope.main_message = "Insignia Editada"
      });
    }, function (response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  } else {
    $http({url: $rootScope.baseURLBackend+'badge-detail/'+$routeParams.badgeId+'/', method: 'PUT', headers: {
    'Authorization': 'Token '+token}, data: $scope.new_badge}).then(
    function successCallback(response) {
      $location.path( "/insignias" );
      $rootScope.main_message = "Insignia Editada"
    });
  }
}

}]);