/*Companies Principal*/
mainApp.controller('Companies', function ($scope, $http, $window, $cookies, $route, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Empresas"
  var token = $cookies.get('tokenpts');

  //Calculating User ID:
  $http({url: $rootScope.baseURLBackend+'rest-auth/user/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $rootScope.userID = response.data['pk']; 
    //Calculating User Profile
    $http({url: $rootScope.baseURLBackend+'get-user-profile/'+$rootScope.userID+'/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
        
       
        $rootScope.userProfile = response.data['results'][0]; 
  //User View Management
  //Change to customize
  if ($rootScope.userProfile.permissionGroup.name == 'Mentores'){
    $http({url: $rootScope.baseURLBackend+'companies-mentor/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
        $scope.message = response.data;
        $scope.companies = response.data['results']; 

        
      }, function errorCallback(response) {});
    } else {

      
      $http({url: $rootScope.baseURLBackend+'companies/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $scope.message = response.data;
          $scope.companies = response.data['results']; 

          

          $scope.delete = function (id) {
            var deleteElement = $window.confirm('¿Seguro que deseas eliminar?');

            if (deleteElement) {
              $http({url: $rootScope.baseURLBackend+'company-detail/'+id+'/', method: 'DELETE', headers: {
                'Authorization': 'Token '+token} }).then(
                function successCallback(response) {
                  $scope.message = response.data;
                  $route.reload();

                }, 
                function errorCallback(response) {});
            }
             
         }
            
          }, function errorCallback(response) {});
      }
    });
});
});

/*Companies Creation*/
mainApp.controller('CompaniesCreate', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', function ($scope, Upload, $timeout, $cookies, $location, $rootScope) {
  
  $rootScope.page_title = "Crear Empresa"
  var token = $cookies.get('tokenpts');


  $scope.send = function(file) {

    if($scope.new_company != undefined && $scope.new_company['name'] != undefined && $scope.new_company['description'] != undefined && file != undefined){
      file.upload = Upload.upload({
        url: $rootScope.baseURLBackend+'company/',
        data: {company: $scope.new_company, file: file},
        headers: {'Authorization': 'Token '+token}
      });

      file.upload.then(function (response) {
        $timeout(function () {
          $location.path( "/empresas" );
          $rootScope.main_message = "Empresa Creada"
        });
      }, function (response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
        // Math.min is to fix IE which reports 200% sometimes
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });
    
    } else {
      $scope.errorMessage = "Debe llenar los campos obligatorios marcados con *";
    }
  }

}]);

/*Companies Edition*/
mainApp.controller('CompanyEdit', ['$scope', 'Upload', '$timeout', '$cookies', '$location', '$rootScope', '$http', '$routeParams', '$route', '$filter', function ($scope, Upload, $timeout, $cookies, $location, $rootScope, $http, $routeParams, $route, $filter) {
  
  $rootScope.page_title = "Editar Empresa"
  var token = $cookies.get('tokenpts');


  $http({url: $rootScope.baseURLBackend+'company-detail/'+$routeParams.companyId+'/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      
      $scope.new_company = response.data;

      $http({url: $rootScope.baseURLBackend+'levels/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $scope.levels = response.data['results']; 
          if ($scope.new_company.level == null){
            $scope.levelId = $scope.levels[0];
          } else {

            var namevar = $filter('filter')($scope.levels, {id: $scope.new_company.level.id})[0];
            $scope.levelId = {id: $scope.new_company.level.id, name: namevar}
          }
        });

        $http({url: $rootScope.baseURLBackend+'templates/', method: 'GET', headers: {
          'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            $scope.templates = response.data['results']; 


            
            if ($scope.new_company.template == null){
              $scope.templateId = $scope.templates[0];
              $scope.loadStages($scope.templates[0].id);
            } else {
              $scope.loadStages($scope.new_company.template.id);
              var namevar = $filter('filter')($scope.templates, {id: $scope.new_company.template.id})[0];
              $scope.templateId = {id: $scope.new_company.template.id, name: namevar}
            }


          });


          $scope.loadStages = function(id) {
            $http({url: $rootScope.baseURLBackend+'stages/'+id+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.stages = response.data['results']; 
                if ($scope.new_company.current_stage_template == null){
                  $scope.currentStageId = $scope.stages[0];
                } else {
                  var namevar = $filter('filter')($scope.stages, {id: $scope.new_company.current_stage_template})[0];
                  $scope.currentStageId = {id: $scope.new_company.current_stage_template, name: namevar}
                }
              }); 
            }

          });




$scope.send = function(file) {
  file = file || 0;

  $scope.new_company['template'] = $scope.templateId.id
  $scope.new_company['level'] = $scope.levelId.id
  $scope.new_company['current_stage_template'] = $scope.currentStageId.id

  if (file!=0){
    file.upload = Upload.upload({
      url: $rootScope.baseURLBackend+'company-detail/'+$routeParams.companyId+'/',
      data: {company: $scope.new_company, file: file},
      headers: {'Authorization': 'Token '+token},
      method: 'PUT'
    });

    file.upload.then(function (response) {
      $timeout(function () {
        $location.path( "/personalizar_tareas/"+$routeParams.companyId );
        $route.reload();
        $rootScope.main_message = "Empresa Editada"
      });
    }, function (response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  } else {
    $http({url: $rootScope.baseURLBackend+'company-detail/'+$routeParams.companyId+'/', method: 'PUT', headers: {
      'Authorization': 'Token '+token}, data: $scope.new_company}).then(
      function successCallback(response) {
        $location.path( "/personalizar_tareas/"+$routeParams.companyId );
        $rootScope.main_message = "Empresa Editada"
      });
    }
  }

}]);


/*Tasks Customization*/
mainApp.controller('TasksCustomization', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $route, $location, $routeParams) {
  
  $rootScope.page_title = "Personalizar Tareas";
  var token = $cookies.get('tokenpts');

  $http({url: $rootScope.baseURLBackend+'company-detail/'+$routeParams.companyId+'/', method: 'GET', headers: {
    'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      
      $scope.new_company = response.data;
      
      $http({url: $rootScope.baseURLBackend+'users/', method: 'GET', headers: {
        'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          
          $scope.users = response.data['results']; 
        }, function errorCallback(response) {});

        $http({url: $rootScope.baseURLBackend+'task-areas/', method: 'GET', headers: {
          'Authorization': 'Token '+token} }).then(
          function successCallback(response) {
            
            $scope.task_areas = response.data['results']; 
          }, function errorCallback(response) {});

          $http({url: $rootScope.baseURLBackend+'status/', method: 'GET', headers: {
            'Authorization': 'Token '+token} }).then(
            function successCallback(response) {
              
              $scope.all_status = response.data['results']; 
            }, function errorCallback(response) {});
            
            $http({url: $rootScope.baseURLBackend+'task-stories-stage-company/'+$scope.new_company.current_stage_template+'/'+$routeParams.companyId+'/', method: 'GET', headers: {
              'Authorization': 'Token '+token} }).then(
              function successCallback(response) {
                $scope.thisStage = $scope.new_company.current_stage_template;
                $scope.message = response.data;
                $scope.task_stories = response.data['results']; 
                $scope.task_story = {start_date: {}, ending_date: {}, points: {}, task_area_id:{}, status_id:{}};

                $scope.delete = function (id) {
                  $http({url: $rootScope.baseURLBackend+'task-detail/'+id, method: 'DELETE', headers: {
                    'Authorization': 'Token '+token} }).then(
                    function successCallback(response) {
                      $route.reload();
                      $rootScope.main_message = "Tarea Eliminada"
                    }, 
                    function errorCallback(response) {});
                    
                  }

                  $scope.save = function(id,index) {      
                    $scope.task_story_post = {start_date: {}, ending_date: {}}; 
                    $scope.task_story_post.start_date = $scope.task_story['start_date'][id];
                    $scope.task_story_post.ending_date = $scope.task_story['ending_date'][id];
                    $scope.task_story_post.points = $scope.task_story['points'][id];
                    if ($scope.task_story['task_area_id'][id]){
                      $scope.task_story_post.task_area_id = $scope.task_story['task_area_id'][id]['id'];
                    }
                    if ($scope.task_story['status_id'][id]){
                      $scope.task_story_post.status_id = $scope.task_story['status_id'][id]['id'];
                    }
                    
                    
                    $scope.task_story_post.in_charge_username = angular.element(document.querySelector('#in_charge_id'+[id]+'_value')).val();
                    


                    $http.put($rootScope.baseURLBackend+'task-story-detail/'+id+'/', $scope.task_story_post, {headers: {
                      'Authorization': 'Token '+token}}).then(
                      function successCallback(response) {
                        $route.reload();
                        $rootScope.main_message = "Empresa Editada"
                      }, function errorCallback(response) {
                        errors = angular.fromJson(response); 
                        console.log(errors['data']);

                      });

                      
                      
                    }
                    
                  }, function errorCallback(response) {});

}
);

});


