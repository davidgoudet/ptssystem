
/*Challenges Principal*/
mainApp.controller('Challenges', function ($scope, $route, $http, $window, $cookies, $cookieStore, $rootScope, $location) {
  
  $rootScope.page_title = "Desafíos"
  var token = $cookies.get('tokenpts');
  
  $http({url: $rootScope.baseURLBackend+'challenges-sent/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
  function successCallback(response) {
    $scope.challenges = response.data['results']; 

    $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'challenge-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $scope.message = response.data;
          $route.reload();

        }, 
        function errorCallback(response) {});
          
    }
    
  }, function errorCallback(response) {});
  
});

  
/*Challenge Creation*/
mainApp.controller('ChallengeCreate', function ($scope, $http, $window, $cookies, $filter, $cookieStore, $rootScope, $location, $routeParams) {
  var token = $cookies.get('tokenpts');
  $rootScope.page_title = "Crear Desafío"
  $scope.new_challenge = {};

  $http({url: $rootScope.baseURLBackend+'users/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
      
      $scope.users = response.data['results']; 
    }, function errorCallback(response) {});

  $http({url: $rootScope.baseURLBackend+'badges/', method: 'GET', headers: {
      'Authorization': 'Token '+token} }).then(
      function successCallback(response) {
      
      $scope.badges = response.data['results']; 
    }, function errorCallback(response) {});

  


  $scope.send = function () {
    if($scope.new_challenge['badge_id'] != undefined && $scope.new_challenge['ending_date'] != undefined  && angular.element(document.querySelector('#in_charge_id_value')).val() != "" && $scope.new_challenge['name'] != undefined && $scope.new_challenge['description'] != undefined){
      $scope.new_challenge.in_charge_username = angular.element(document.querySelector('#in_charge_id_value')).val();
      $scope.new_challenge.badge_id = $scope.new_challenge['badge_id']['id'];
      $http.post($rootScope.baseURLBackend+'challenge/', $scope.new_challenge, {headers: {
      'Authorization': 'Token '+token}}).then(
      function successCallback(response) {
        $location.path( "/desafios/" );
        $rootScope.main_message = "Desafío Creado"
      }, function errorCallback(response) {
          errors = angular.fromJson(response); 
          console.log(errors['data']);

      });
    } else {
      $scope.errorMessage = "Debe llenar todos los campos";
    }
  }


});



/*Challenges Edition*/
mainApp.controller('ChallengeEdit', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams, $filter) {
  
  $rootScope.page_title = "Editar Desafío"
  var token = $cookies.get('tokenpts');

  
 
  $http({url: $rootScope.baseURLBackend+'challenge-detail/'+$routeParams.challengeId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.new_challenge = response.data;

    

      $scope.send = function () {
        

        $http.put($rootScope.baseURLBackend+'challenge-detail/'+$routeParams.challengeId+'/', $scope.new_challenge, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/desafios/" ); 
          $rootScope.main_message = "Desafío Editado"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);

        });
      }

    

      $scope.delete = function (id) {
      $http({url: $rootScope.baseURLBackend+'challenge-detail/'+id+'/', method: 'DELETE', headers: {
      'Authorization': 'Token '+token} }).then(
        function successCallback(response) {
          $location.path( "/desafios/"+challengeIdOriginal );
          $rootScope.main_message = "Desafío Eliminado"
        }, 
        function errorCallback(response) {});
          
      }

    }
  )

  
  
  
});

/*Challenges Review*/
mainApp.controller('ChallengeReview', function ($scope, $http, $window, $cookies, $cookieStore, $rootScope, $location, $routeParams, $filter) {
  
  $rootScope.page_title = "Revisar Desafío"
  var token = $cookies.get('tokenpts');

  
 
  $http({url: $rootScope.baseURLBackend+'challenge-detail/'+$routeParams.pendingObjectId+'/', method: 'GET', headers: {
  'Authorization': 'Token '+token} }).then(
    function successCallback(response) {
      $scope.pending_object = response.data;
            

      

    

      $scope.send = function () {
       

        $http.put($rootScope.baseURLBackend+'challenge-review/'+$routeParams.pendingObjectId+'/', $scope.new_challenge, {headers: {
        'Authorization': 'Token '+token}}).then(
          function successCallback(response) {
          $location.path( "/pendientes" ); 
          $rootScope.main_message = "Entrega Revisada"
          }, function errorCallback(response) {
            errors = angular.fromJson(response); 
            console.log(errors['data']);;

        });
      }

    


    }
  )

  
  
  
});
